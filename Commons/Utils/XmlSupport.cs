// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Xml;

namespace Celsio.Commons.Utils
{
	public static class XmlSupport
	{
		public static bool TryGetXmlNodeInnerText(XmlNode node, string name, out string value)
		{
			value = String.Empty;
			XmlNode xmlNode = node.SelectSingleNode(name);
			if (xmlNode == null)
				return false;
			value = xmlNode.InnerText;
			return true;
		}

		public static string SafelyGetXmlNodeInnerText(XmlNode node, string name)
		{
			XmlNode xmlNode = node.SelectSingleNode(name);
			if (xmlNode == null)
				throw new Exception("Не найден узел " + name);
			return xmlNode.InnerText;
		}

		public static string SafelyGetXmlNodeInnerText(XmlNode node, string name, string defValue)
		{
			XmlNode xmlNode = node.SelectSingleNode(name);
			return xmlNode == null ? defValue : xmlNode.InnerText;
		}

		public static XmlNode SafelyGetSubNode(XmlNode node, string name)
		{
			XmlNode sn = node.SelectSingleNode(name);
			if (sn == null)
				throw new Exception("Не найден узел " + name);
			return sn;
		}

		public static XmlAttributeCollection ExtractXmlAttributesWithException(XmlNode node)
		{
			XmlAttributeCollection attributes = node.Attributes;
			if (attributes == null)
				throw new Exception("Ошибка доступа к списку атрибутов");
			return attributes;
		}

		public static string ExtractAttributeValueWithException(XmlAttributeCollection attributes, string name)
		{
			XmlAttribute atr = attributes[name];
			if (atr == null) throw new Exception("Не найден атрибут: " + name);
			return atr.Value;
		}

		public static string ExtractAttributeValue(XmlAttributeCollection attributes, string name, string defValue)
		{
			XmlAttribute atr = attributes[name];
			return atr == null ? defValue : atr.Value;
		}

		public static void WriteElementValue<T>(XmlWriter writer, string name, T value)
		{
			writer.WriteStartElement(name);
			writer.WriteValue(value);
			writer.WriteEndElement();
		}
	}
}
