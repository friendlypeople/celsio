// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Celsio.Commons.Utils
{
	public interface IAbstractBinaryElementFormatter
	{
		long Write(Stream stream, object obj);
		object Read(Stream stream);
	}

	class Int32BinaryFormatter : IAbstractBinaryElementFormatter
	{
		const int Size = sizeof(Int32);

		public long Write(Stream stream, object obj)
		{
			byte[] buffer = BitConverter.GetBytes((Int32)obj);
			stream.Write(buffer, 0, Size);
			return Size;
		}

		public object Read(Stream stream)
		{
			byte[] buffer = new byte[Size];
			stream.Read(buffer, 0, Size);
			return BitConverter.ToInt32(buffer, 0);
		}
	}

	class Int16BinaryFormatter : IAbstractBinaryElementFormatter
	{
		const int Size = sizeof(Int16);

		public long Write(Stream stream, object obj)
		{
			byte[] buffer = BitConverter.GetBytes((Int16)obj);
			stream.Write(buffer, 0, Size);
			return Size;
		}

		public object Read(Stream stream)
		{
			byte[] buffer = new byte[Size];
			stream.Read(buffer, 0, Size);
			return BitConverter.ToInt16(buffer, 0);
		}
	}

	class Int64BinaryFormatter : IAbstractBinaryElementFormatter
	{
		const int Size = sizeof(Int64);

		public long Write(Stream stream, object obj)
		{
			byte[] buffer = BitConverter.GetBytes((Int64)obj);
			stream.Write(buffer, 0, Size);
			return Size;
		}

		public object Read(Stream stream)
		{
			byte[] buffer = new byte[Size];
			stream.Read(buffer, 0, Size);
			return BitConverter.ToInt64(buffer, 0);
		}
	}

	class UInt16BinaryFormatter : IAbstractBinaryElementFormatter
	{
		const int Size = sizeof(UInt16);

		public long Write(Stream stream, object obj)
		{
			byte[] buffer = BitConverter.GetBytes((UInt16)obj);
			stream.Write(buffer, 0, Size);
			return Size;
		}

		public object Read(Stream stream)
		{
			byte[] buffer = new byte[Size];
			stream.Read(buffer, 0, Size);
			return BitConverter.ToUInt16(buffer, 0);
		}
	}

	class UInt32BinaryFormatter : IAbstractBinaryElementFormatter
	{
		const int Size = sizeof(UInt32);

		public long Write(Stream stream, object obj)
	    {
			byte[] buffer = BitConverter.GetBytes((UInt32)obj);
			stream.Write(buffer, 0, Size);
			return Size;
	    }

		public object Read(Stream stream)
	    {
			byte[] buffer = new byte[Size];
			stream.Read(buffer, 0, Size);
			return BitConverter.ToUInt32(buffer, 0);
	    }
	}

	class UInt64BinaryFormatter : IAbstractBinaryElementFormatter
	{
		const int Size = sizeof(UInt64);

		public long Write(Stream stream, object obj)
		{
			byte[] buffer = BitConverter.GetBytes((UInt64)obj);
			stream.Write(buffer, 0, Size);
			return Size;
		}

		public object Read(Stream stream)
		{
			byte[] buffer = new byte[Size];
			stream.Read(buffer, 0, Size);
			return BitConverter.ToUInt64(buffer, 0);
		}
	}

	class StringBinaryFormatter : IAbstractBinaryElementFormatter
	{
		const int IntSize = sizeof(int);

		public long Write(Stream stream, object obj)
		{
			byte[] buffer = Encoding.Default.GetBytes((string)obj);
			int legth = buffer.Length;
			byte[] sizeBuffer = BitConverter.GetBytes(legth);
			stream.Write(sizeBuffer, 0, IntSize);
			stream.Write(buffer, 0, legth);
			return legth + IntSize;
		}

		public object Read(Stream stream)
		{
			byte[] sizeBuffer = new byte[IntSize];
			stream.Read(sizeBuffer, 0, sizeBuffer.Length);
			int size = BitConverter.ToInt32(sizeBuffer, 0);
			byte[] buffer = new byte[size];
			stream.Read(buffer, 0, size);
			return Encoding.Default.GetString(buffer);
		}
	}

	class SingleBinaryFormatter : IAbstractBinaryElementFormatter
	{
		const int Size = sizeof(Single);

		public long Write(Stream stream, object obj)
		{
			byte[] buffer = BitConverter.GetBytes((Single)obj);
			stream.Write(buffer, 0, Size);
			return Size;
		}

		public object Read(Stream stream)
		{
			byte[] buffer = new byte[Size];
			stream.Read(buffer, 0, Size);
			return BitConverter.ToSingle(buffer, 0);
		}
	}

	class SByteBinaryFormatter : IAbstractBinaryElementFormatter
	{
		const int Size = sizeof(SByte);

		public long Write(Stream stream, object obj)
		{
			byte[] buffer = BitConverter.GetBytes((SByte)obj);
			stream.Write(buffer, 0, Size);
			return Size;
		}

		public object Read(Stream stream)
		{
			byte buffer = (byte)stream.ReadByte();
			return (SByte)buffer;
		}
	}

	class DoubleBinaryFormatter : IAbstractBinaryElementFormatter
	{
		const int Size = sizeof(Double);

		public long Write(Stream stream, object obj)
		{
			byte[] buffer = BitConverter.GetBytes((Double)obj);
			stream.Write(buffer, 0, Size);
			return Size;
		}

		public object Read(Stream stream)
		{
			byte[] buffer = new byte[Size];
			stream.Read(buffer, 0, Size);
			return BitConverter.ToDouble(buffer, 0);
		}
	}

	class DecimalBinaryFormatter : IAbstractBinaryElementFormatter
	{
		const int IntSize = sizeof(int);
		const int Count = sizeof(Decimal) / sizeof(int);

		public long Write(Stream stream, object obj)
		{
			int[] res = decimal.GetBits((decimal)obj);
			foreach (int i in res)
			{
				byte[] buffer = BitConverter.GetBytes(i);
				stream.Write(buffer, 0, IntSize);
			}
			return IntSize * Count;
		}

		public object Read(Stream stream)
		{
			int[] fints = new int[Count];
			for (int i = 0; i < Count; i++)
			{
				byte[] buffer = new byte[IntSize];
				stream.Read(buffer, 0, IntSize);
				fints[i] = BitConverter.ToInt32(buffer, 0);
			}
			return new decimal(fints);
		}
	}

	class CharBinaryFormatter : IAbstractBinaryElementFormatter
	{
		const int Size = sizeof(Char);

		public long Write(Stream stream, object obj)
		{
			byte[] buffer = BitConverter.GetBytes((Char)obj);
			stream.Write(buffer, 0, Size);
			return Size;
		}

		public object Read(Stream stream)
		{
			byte[] buffer = new byte[Size];
			stream.Read(buffer, 0, Size);
			return BitConverter.ToChar(buffer, 0);
		}
	}

	class ByteBinaryFormatter : IAbstractBinaryElementFormatter
	{
		public long Write(Stream stream, object obj)
		{
			stream.WriteByte((byte)obj);
			return 1;
		}

		public object Read(Stream stream)
		{
			return (byte)stream.ReadByte();
		}
	}

	class BooleanBinaryFormatter : IAbstractBinaryElementFormatter
	{
		public long Write(Stream stream, object obj)
		{
			byte by = (byte)((bool)obj ? 1 : 0);
			stream.WriteByte(by);
			return 1;
		}

		public object Read(Stream stream)
		{
			return (byte)stream.ReadByte() == 1;
		}
	}

	public class BinaryAccessMechanism
	{
		private Stream _stream;
		private static readonly Dictionary<Type, IAbstractBinaryElementFormatter> WriterTable = new Dictionary<Type, IAbstractBinaryElementFormatter>(10);

		static BinaryAccessMechanism()
		{
			WriterTable.Add(typeof(Int16), new Int16BinaryFormatter());
			WriterTable.Add(typeof(Int32), new Int32BinaryFormatter());
			WriterTable.Add(typeof(Int64), new Int64BinaryFormatter());
			WriterTable.Add(typeof(UInt16), new UInt16BinaryFormatter());
			WriterTable.Add(typeof(UInt32), new UInt32BinaryFormatter());
			WriterTable.Add(typeof(UInt64), new UInt64BinaryFormatter());

			WriterTable.Add(typeof(String), new StringBinaryFormatter());
			WriterTable.Add(typeof(Single), new SingleBinaryFormatter());
			WriterTable.Add(typeof(SByte), new SByteBinaryFormatter());
			WriterTable.Add(typeof(Double), new DoubleBinaryFormatter());
			WriterTable.Add(typeof(Decimal), new DecimalBinaryFormatter());

			WriterTable.Add(typeof(Char), new CharBinaryFormatter());
			WriterTable.Add(typeof(Boolean), new BooleanBinaryFormatter());
			WriterTable.Add(typeof(Byte), new ByteBinaryFormatter());
		}

		public static void AddFormatter(Type type, IAbstractBinaryElementFormatter formatter)
		{
			WriterTable[type] = formatter;
		}

        public static void AddFormatter<T, TFormatter>() where TFormatter : IAbstractBinaryElementFormatter, new()
        {
            Type type = typeof(T);
            IAbstractBinaryElementFormatter formatter = new TFormatter();
            WriterTable[type] = formatter;
        }

		public BinaryAccessMechanism(Stream stream)
		{
			_stream = stream;
		}

		public long Write(object obj)
		{
			Type type = obj.GetType();
			return Write(obj, type);
		}

		public long Write(object obj, Type type)
		{
			if (WriterTable.ContainsKey(type))
			{
				return WriterTable[type].Write(_stream, obj);
			}
			throw new Exception("�� ������ �������� ��������� ��� ���� " + type.Name);
		}

		public long Write<T>(object obj)
		{
			return Write(obj, typeof(T));
		}

		public object Read(Type type)
		{
			if (WriterTable.ContainsKey(type))
				return WriterTable[type].Read(_stream);
			throw new Exception("�� ������ �������� ��������� ��� ���� " + type.Name);
		}

		public T Read<T>()
		{
			return (T)Read(typeof(T));
		}

		public void SelectStream(Stream stream)
		{
			_stream = stream;
		}

		public void Write(Stream stream)
		{
			stream.Position = 0;
			CopyStream(stream, _stream);
		}

		public void SkipNotExistingField()
		{
			long size = Read<long>();
			_stream.Position += size;
		}
		private void CopyStream(Stream src, Stream dest)
		{
			byte[] buffer = new byte[1024];
			int len = src.Read(buffer, 0, buffer.Length);
			while (len > 0)
			{
				dest.Write(buffer, 0, len);
				len = src.Read(buffer, 0, buffer.Length);
			}
			//dest.Flush();
		}
	}
}

