// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Reflection;
using System.Text;

namespace Celsio.Commons.Utils
{
	public static class ReflectionSupport
	{
		public static string GetGetSignature(PropertyInfo src)
		{
			return GetSignature(src.GetGetMethod());
		}

		public static string GetSetSignature(PropertyInfo src)
		{
			return GetSignature(src.GetSetMethod());
		}

		public static string GetSignature(MethodInfo src)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append(src.ReturnType.FullName + " ");
			sb.Append(src.Name);
			if (src.IsGenericMethod)
			{
				sb.Append("[");
				Type[] arguments = src.GetGenericArguments();
				for (int index = 0; index < arguments.Length; index++)
				{
					Type t = arguments[index];
					sb.Append(t.Name);
					if (index != arguments.Length - 1)
						sb.Append(",");
				}
				sb.Append("]");
			}
			sb.Append("(");
			ParameterInfo[] infos = src.GetParameters();
			sb.Append(GetParametersSignature(infos));
			sb.Append(")");
			return sb.ToString();
		}

		public static bool IsPropertyMethod(MethodInfo src)
		{
			return src.Name.StartsWith("get_") || src.Name.StartsWith("set_");
		}

		internal static bool EqualsImpl(MethodInfo src, MethodInfo dst)
		{
			return GetSignature(src).Equals(GetSignature(dst));
		}

		internal static string GetParametersSignature(ParameterInfo[] infos)
		{
			StringBuilder sb = new StringBuilder();
			for (int index = 0; index < infos.Length; index++)
			{
				ParameterInfo parameter = infos[index];

				Type parameterType = parameter.ParameterType;
				sb.Append(parameterType.IsGenericParameter ? parameterType.Name : parameterType.FullName);
				if (index != infos.Length - 1)
					sb.Append(",");
			}
			return sb.ToString();
		}
	}
}
