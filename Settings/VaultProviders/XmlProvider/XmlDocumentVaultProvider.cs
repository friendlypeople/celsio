// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.IO;
using System.Xml;

namespace Celsio.Settings.VaultProviders.XmlProvider
{
    public class XmlDocumentVaultProvider : IVaultProvider
    {
        private static readonly SettingsContainerXmlSerializer XmlSerializer = new SettingsContainerXmlSerializer();
		protected const string RootName		= "SettingsContainer";

		private readonly XmlDocument _doc;
		private readonly string _vaultName;
		private XmlWriterSettings _settings = new XmlWriterSettings();

		/// <summary>
		/// Инициализация экземпляра класса <see cref="XmlVaultProvider"/>.
		/// </summary>
        /// <param name="doc">Документ.</param>
        public XmlDocumentVaultProvider(XmlDocument doc)
		{
            if (doc == null) throw new ArgumentNullException("doc");
            _doc = doc;

			XmlSettings.Indent = true;
		}

		/// <summary>
		/// Инициализация экземпляра класса <see cref="XmlVaultProvider"/>.
		/// </summary>
        /// <param name="doc">Xml document.</param>
		/// <param name="vaultName">Name of the vault.</param>
        public XmlDocumentVaultProvider(XmlDocument doc, string vaultName)
            : this(doc)
		{
			_vaultName = vaultName;
		}


		public XmlWriterSettings XmlSettings
		{
			get { return _settings; }
			set
			{
				if (_settings == null)
					throw new ArgumentNullException("value");
				_settings = value; 
			}
		}

		#region Implementation of IVaultProvider

		public void Save(SettingsContainer container)
		{
			if (container == null) throw new ArgumentNullException("container");
            MemoryStream stream = new MemoryStream();
            XmlWriter writer = InitializeWriter(stream);
			Save(writer, container);
			CloseWriter(writer);
            stream.Position = 0;
            _doc.Load(stream);
            stream.Close();
		}

		public void Save(SettingsManager manager)
		{
			if (manager == null) throw new ArgumentNullException("manager");
            MemoryStream stream = new MemoryStream();
            XmlWriter writer = InitializeWriter(stream);
			foreach (SettingsContainer container in manager.Items.Values)
			{
				Save(writer, container);
			}
			CloseWriter(writer);
            stream.Position = 0;
            _doc.Load(stream);
            stream.Close();
		}

		public void Load(SettingsContainer container)
		{
			if (container == null) throw new ArgumentNullException("container");
			XmlNode node = InitializeXmlDoc();
			Load(node, container);
		}

		public void Load(SettingsManager manager)
		{
			if (manager == null) throw new ArgumentNullException("manager");
			XmlNode node = InitializeXmlDoc();
			foreach (XmlNode chNode in node.ChildNodes)
			{
				if (manager.Items.ContainsKey(chNode.Name))
					Load(node, manager.Items[chNode.Name]);
			}
		}

		#endregion

        private void Save(XmlWriter writer, SettingsContainer container)
        {
            XmlSerializer.Save(writer, container);
        }

        private void Load(XmlNode node, SettingsContainer container)
        {
            XmlSerializer.Load(node, container);
        }

		private XmlNode InitializeXmlDoc()
		{
			string vaultName = String.IsNullOrEmpty(_vaultName) ? RootName : _vaultName;

			XmlNode node = _doc.SelectSingleNode(vaultName);
			if (node == null) throw new Exception("Не найден узел " + vaultName);
			return node;
		}

		private XmlWriter InitializeWriter(Stream stream)
		{
            XmlWriter writer = XmlWriter.Create(stream, _settings);
			if (writer == null) throw new Exception("Не удалось создать XmlWriter");
			writer.WriteStartDocument();
			string vaultName = String.IsNullOrEmpty(_vaultName) ? RootName : _vaultName;
			writer.WriteStartElement(vaultName);
			return writer;
		}

		private void CloseWriter(XmlWriter writer)
		{
			writer.WriteEndElement();
            writer.WriteEndDocument();
			writer.Close();
		}
    }
}
