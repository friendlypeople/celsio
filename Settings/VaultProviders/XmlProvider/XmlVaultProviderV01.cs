// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Xml;
using Celsio.Data;
using Celsio.Serializers;

namespace Celsio.Settings.VaultProviders.XmlProvider
{
	/// <summary>
	/// Базовая реализация сервиса сохранения настроек в Xml
	/// </summary>
	internal sealed class XmlVaultProviderV01 : IXmlVersionVaultProvider
	{
		private static readonly DbRecordDataSerializer StaticSerializer = new DbRecordDataSerializer();
		private static readonly DynDbRecordDataSerializer DynamicSerializer = new DynDbRecordDataSerializer();

		

		#region Implementation of IVaultProvider

		public void Save(XmlWriter writer, SettingsContainer container)
		{
			writer.WriteAttributeString("publicName", container.PublicName);
			
			writer.WriteStartElement("Settings");
			foreach (DbRecordData data in container.Settings.Values)
			{
				if (container.IsDynamic)
					DynamicSerializer.Save(writer, data);
				else
					StaticSerializer.Save(writer, data);
			}
			writer.WriteEndElement();

			writer.WriteStartElement("Groups");
			foreach (SettingsContainer group in container.Groups.Values)
			{
				writer.WriteStartElement(group.PrivateName);
				Save(writer, group);
				writer.WriteEndElement();
			}
			writer.WriteEndElement();
		}

		public void Load(XmlNode node, SettingsContainer container)
		{
			if (node == null) throw new ArgumentNullException("node");
			if (node.Attributes == null)
				throw new Exception("Не верный формат данных в Xml: не найдены атрибуты");
			
			XmlAttribute pn = node.Attributes["publicName"];
			if (pn != null)
				container.PublicName = pn.Value;

			XmlNode chNode = node.SelectSingleNode("Settings");
			if (chNode == null)
				throw new Exception("Не верный формат данных в Xml: не найдены узел Settings");

			foreach (XmlNode childNode in chNode.ChildNodes)
			{
				string key = childNode.Name;
				if (container.Settings.ContainsKey(key))
				{
					if (container.IsDynamic)
						DynamicSerializer.Load(childNode, container.Settings[key]);
					else
						StaticSerializer.Load(childNode, container.Settings[key]);
				}
			}

			chNode = node.SelectSingleNode("Groups");
			if (chNode == null)
				throw new Exception("Не верный формат данных в Xml: не найдены узел Groups");

			foreach (XmlNode childNode in chNode.ChildNodes)
			{
				if (container.Groups.ContainsKey(childNode.Name))
					Load(childNode, container.Groups[childNode.Name]);
			}
		}

		#endregion

		#region Implementation of IVersion

		public string Version
		{
			get { return "0.1"; }
		}

		#endregion
	}
}
