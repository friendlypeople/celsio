// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.IO;
using Celsio.Data;
using Celsio.Runtime.Converters;
using Microsoft.Win32;

namespace Celsio.Settings.VaultProviders.RegistryProvider
{
	internal sealed class ExactProviderV01 : IRegistryVersionVaultProvider
	{
		private void SaveDbRecordDataToReg(RegistryKey key, DbRecordData data)
		{
			RegistryKey registryKey = key.CreateSubKey(data.PrivateName);
			if (registryKey == null) throw new Exception("Не удалось создать ключ " + key.Name + data.PrivateName);

			registryKey.SetValue("publicName", data.PublicName);

			if (data.Type != DbDataType.Blob)
				registryKey.SetValue("value", TypeConverterProvider.SpecificConvertToString(data.SystemType, data.Value));
			else
				registryKey.SetValue("value", data.Value, RegistryValueKind.Binary);

			RegistryKey subKey = registryKey.CreateSubKey("atrributes");
			if (subKey == null) throw new Exception("Не удалось создать ключ " + registryKey.Name + "atrributes");
			foreach (Pair<string, string> attribute in data.Attributes)
				subKey.SetValue(attribute.First, attribute.Second);
			subKey.Close();

			subKey = registryKey.CreateSubKey("items");
			if (subKey == null) throw new Exception("Не удалось создать ключ " + registryKey.Name + "items");
			foreach (DbRecordData item in data.Items)
				SaveDbRecordDataToReg(registryKey, item);
			subKey.Close();
			registryKey.Close();
		}

		private void LoadDbRecordDataFromReg(RegistryKey key, DbRecordData data)
		{
			//RegistryKey subKey = key.OpenSubKey(data.PrivateName);
			//if (subKey == null) throw new Exception("Не удалось открыть ключ " + key.Name + data.PrivateName);

			string str = key.GetValue("publicName") as string;
			if (!string.IsNullOrEmpty(str))
				data.PublicName = str;
			if (data.Type != DbDataType.Blob)
			{
				str = key.GetValue("value") as String;
				if (string.IsNullOrEmpty(str))
					throw new Exception("Не верный формат данных в реестре: не найдены узел value");

				object val = TypeConverterProvider.SpecificConvertFromString(data.SystemType, str);
				data.Value = val;
			}
			else
			{
				byte[] blob = key.GetValue("value", null) as byte[];
				if (blob == null)
					throw new Exception("Не верный формат данных в реестре: не найдены узел value");
				data.Value = blob;
			}

			RegistryKey sKey = key.OpenSubKey("atrributes");
			if (sKey == null) throw new Exception("Не удалось открыть ключ " + key.Name + "atrributes");

			data.Attributes.Clear();
			foreach (string attr in sKey.GetValueNames())
			{
				string atval = sKey.GetValue(attr, "") as string;
                data.Attributes.Add(attr, atval);
			}
			sKey.Close();

			sKey = key.OpenSubKey("items");
			if (sKey == null) throw new Exception("Не удалось открыть ключ " + key.Name + "items");

			foreach (string attr in sKey.GetSubKeyNames())
			{
				foreach (DbRecordData item in data.Items)
				{
					if (item.PrivateName == attr)
					{
						LoadDbRecordDataFromReg(sKey, item);
						break;
					}
				}
			}
			sKey.Close();
		}

		#region Implementation of IVersion

		public string Version { get { return "0.1"; } }

		#endregion

		#region Implementation of IRegistryVersionVaultProvider

		public void Save(RegistryKey key, SettingsContainer container)
		{
			if (key == null) throw new ArgumentNullException("key");
			if (container == null) throw new ArgumentNullException("container");

			key.SetValue("publicName", container.PublicName);

			RegistryKey registryKey = key.CreateSubKey("Settings");
			if (registryKey == null) throw new Exception("Не удалось создать ключ " + key.Name + @"\Settings");

			foreach (DbRecordData data in container.Settings.Values)
				SaveDbRecordDataToReg(registryKey, data);

			registryKey.Close();

			registryKey = key.CreateSubKey("Groups");
			if (registryKey == null) throw new Exception("Не удалось создать ключ " + key.Name + @"\Groups");
			foreach (SettingsContainer group in container.Groups.Values)
			{
				RegistryKey subKey = registryKey.CreateSubKey(group.PrivateName);
				if (subKey == null) throw new Exception("Не удалось создать ключ " + key.Name + container.PrivateName);
				Save(subKey, group);
				subKey.Close();
			}
			registryKey.Close();
			
		}

		public void Load(RegistryKey key, SettingsContainer container)
		{
			if (key == null) throw new ArgumentNullException("key");
			if (container == null) throw new ArgumentNullException("container");
			
			string pn = key.GetValue("publicName") as string;
			if (!string.IsNullOrEmpty(pn))
				container.PublicName = pn;

			RegistryKey subKey = key.OpenSubKey("Settings");
			if (subKey == null) throw new Exception("Не верный формат данных: не найдены узел Settings");

			foreach (string name in subKey.GetSubKeyNames())
			{
				RegistryKey skey = subKey.OpenSubKey(name);
				if (skey == null) throw new Exception("Не удалось открыть ключ " + key.Name + @"\" + name);
				string keyName = Path.GetFileName(name);
				if (container.Settings.ContainsKey(keyName))
					LoadDbRecordDataFromReg(skey, container.Settings[keyName]);
				skey.Close();
			}
			subKey.Close();

			subKey = key.OpenSubKey("Groups");
			if (subKey == null) throw new Exception("Не верный формат данных: не найдены узел Groups");

			foreach (string name in subKey.GetSubKeyNames())
			{
				RegistryKey skey = subKey.OpenSubKey(name);
				if (skey == null) throw new Exception("Не удалось открыть ключ " + key.Name + @"\" + name);
				string keyName = Path.GetFileName(name);
				if (container.Groups.ContainsKey(keyName))
					Load(skey, container.Groups[keyName]);
				skey.Close();
			}
			subKey.Close();
		}

		#endregion
	}
}
