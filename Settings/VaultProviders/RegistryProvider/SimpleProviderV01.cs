// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Celsio.Data;
using Celsio.Runtime.Converters;
using Microsoft.Win32;

namespace Celsio.Settings.VaultProviders.RegistryProvider
{
	internal sealed class SimpleProviderV01 : IRegistryVersionVaultProvider
	{
		private void SaveDbRecordDataToReg(RegistryKey key, DbRecordData data)
		{
			if (data.Type != DbDataType.Blob)
				key.SetValue(data.PrivateName, TypeConverterProvider.SpecificConvertToString(data.SystemType, data.Value));
			else
				key.SetValue(data.PrivateName, data.Value, RegistryValueKind.Binary);
		}

		private void LoadDbRecordDataFromReg(RegistryKey key, DbRecordData data)
		{
			if (data.Type != DbDataType.Blob)
			{
				string str = key.GetValue(data.PrivateName) as String;
				if (!string.IsNullOrEmpty(str))
				{
					object val = TypeConverterProvider.SpecificConvertFromString(data.SystemType, str);
					data.Value = val;
				}
			}
			else
			{
				byte[] blob = key.GetValue(data.PrivateName, null) as byte[];
				if (blob != null)
					data.Value = blob;
			}
		}

		#region Implementation of IVersion

		public string Version { get { return "0.1"; } }

		#endregion

		#region Implementation of IRegistryVersionVaultProvider

		public void Save(RegistryKey key, SettingsContainer container)
		{
			if (key == null) throw new ArgumentNullException("key");
			if (container == null) throw new ArgumentNullException("container");

			foreach (DbRecordData data in container.Settings.Values)
				SaveDbRecordDataToReg(key, data);

			foreach (SettingsContainer group in container.Groups.Values)
			{
				RegistryKey subKey = key.CreateSubKey(group.PrivateName);
				if (subKey == null) throw new Exception("Не удалось создать ключ " + key.Name + container.PrivateName);
				Save(subKey, group);
				subKey.Close();
			}
		}

		public void Load(RegistryKey key, SettingsContainer container)
		{
			if (key == null) throw new ArgumentNullException("key");
			if (container == null) throw new ArgumentNullException("container");

			foreach (string name in key.GetValueNames())
			{
				if (container.Settings.ContainsKey(name))
					LoadDbRecordDataFromReg(key, container.Settings[name]);
			}

			foreach (string name in key.GetSubKeyNames())
			{
				RegistryKey skey = key.OpenSubKey(name);
				if (skey == null) throw new Exception("Не удалось открыть ключ " + key.Name + @"\" + name);
				if (container.Groups.ContainsKey(name))
					Load(skey, container.Groups[name]);
				skey.Close();
			}
		}

		#endregion
	}
}
