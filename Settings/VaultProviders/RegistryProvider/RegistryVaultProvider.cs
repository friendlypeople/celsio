// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.IO;
using Celsio.Patterns.VersionController;
using Microsoft.Win32;

namespace Celsio.Settings.VaultProviders.RegistryProvider
{
	public enum HiveType
	{
		LocalMachine,
		CurrentUser
	}

	/// <summary>
	/// Сервис сохранения настроек в реестр
	/// </summary>
	public abstract class RegistryVaultProvider : IVaultProvider
	{
		private readonly HiveType	_hive = HiveType.LocalMachine;
		private readonly string		_keyPath = string.Empty;

		protected static readonly VersionController<IRegistryVersionVaultProvider> VersionController =
			new VersionController<IRegistryVersionVaultProvider>();


		protected RegistryVaultProvider(HiveType hive, string keyPath)
		{
			_hive = hive;
			_keyPath = keyPath;
		}

		private RegistryKey InitializeKey()
		{
			RegistryKey key = _hive == HiveType.CurrentUser ? Registry.CurrentUser : Registry.LocalMachine;
			RegistryKey subKey = key.CreateSubKey(_keyPath);
			if (subKey == null)
				throw new Exception("Не удалось создать ключ " + key.Name + @"\" + _keyPath);
			return subKey;
		}

		public void Save(RegistryKey key, SettingsContainer container)
		{
			if (key == null) throw new ArgumentNullException("key");
			if (container == null) throw new ArgumentNullException("container");

			IRegistryVersionVaultProvider provider = VersionController.GetVersionMajor();
			if (provider == null) throw new Exception("Не найдена старшая версия в контроллере");

			key.SetValue("", provider.Version);
			provider.Save(key, container);
		}

		public void Load(RegistryKey key, SettingsContainer container)
		{
			if (key == null) throw new ArgumentNullException("key");
			if (container == null) throw new ArgumentNullException("container");

			string value = key.GetValue("") as string;
			if  (String.IsNullOrEmpty(value)) throw new Exception("Не найдена версия");

			IRegistryVersionVaultProvider provider = VersionController.GetVersion(value);
			if (provider == null) throw new Exception("Не найдена версия " + value + " в контроллере");

			provider.Load(key, container);
			
		}

		#region Implementation of IVaultProvider

		public void Save(SettingsContainer container)
		{
			if (container == null) throw new ArgumentNullException("container");
			RegistryKey key = InitializeKey();
			RegistryKey subKey = key.CreateSubKey(container.PrivateName);
			if (subKey == null) throw new Exception("Не удалось создать ключ " + key.Name + @"\" + container.PrivateName);

			Save(subKey, container);
			subKey.Close();
			key.Close();
		}

		public void Save(SettingsManager manager)
		{
			if (manager == null) throw new ArgumentNullException("manager");
			RegistryKey key = InitializeKey();
			foreach (SettingsContainer container in manager.Items.Values)
			{
				RegistryKey subKey = key.CreateSubKey(container.PrivateName);
				if (subKey == null) throw new Exception("Не удалось создать ключ " + key.Name + @"\" + container.PrivateName);

				Save(subKey, container);
				subKey.Close();
			}
			key.Close();
		}

		public void Load(SettingsContainer container)
		{
			if (container == null) throw new ArgumentNullException("container");
			RegistryKey key = InitializeKey();

			RegistryKey subKey = key.OpenSubKey(container.PrivateName);
			if (subKey == null) throw new Exception("Не удалось создать ключ " + key.Name + @"\" + container.PrivateName);
			Load(subKey, container);
			subKey.Close();
		}

		public void Load(SettingsManager manager)
		{
			if (manager == null) throw new ArgumentNullException("manager");
			RegistryKey key = InitializeKey();
			foreach (string name in key.GetSubKeyNames())
			{
				RegistryKey subKey = key.OpenSubKey(name);
				if (subKey == null) continue;

				string keyName = Path.GetFileName(subKey.Name);
				if (manager.Items.ContainsKey(keyName))
					Load(subKey, manager.Items[keyName]);
				subKey.Close();
			}
		}
		#endregion
	}
}
