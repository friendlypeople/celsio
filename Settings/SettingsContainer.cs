// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using Celsio.Data;
using Celsio.Serializers.AbnormalSerializer;

namespace Celsio.Settings
{
    [AbnormalSerialize]
	public class SettingsContainer
	{
        [SerializeDeclarate]
		private string _privateName = string.Empty;

        [SerializeDeclarate]
		private string _publicName = string.Empty;

		private Type _graphicElement;

		private Guid _guid = Guid.NewGuid();

        [SerializeDeclarate]
		private bool _isDynamic;

        [SerializeDeclarate]
		private readonly Dictionary<string, SettingsContainer> _groups = 
			new Dictionary<string, SettingsContainer>();

        [SerializeDeclarate]
		private readonly Dictionary<string, DbRecordData> _settings =
			new Dictionary<string, DbRecordData>();

		/// <summary>
		///Создание нового экземпляра класса <see cref="SettingsContainer"/>.
		/// </summary>
		public SettingsContainer() { }

		/// <summary>
		/// Создание нового экземпляра класса <see cref="SettingsContainer"/>.
		/// </summary>
		/// <param name="privateName">Внутреннее имя контейнера.</param>
		public SettingsContainer(string privateName)
		{
			_privateName = privateName;
		}

		public SettingsContainer(SettingsContainer src)
		{
			CloneImplementation(src, this);
		}

		/// <summary>
		/// Получить или задать внутренне имя контейнера
		/// </summary>
		/// <value>
		/// Внутренне имя контейнера.
		/// </value>
		public string PrivateName
		{
			get { return _privateName; }
			set { _privateName = value; }
		}

		/// <summary>
		/// Получить или задать отображаемое имя контейнера
		/// </summary>
		/// <value>Отображаемое имя контейнера</value>
		public string PublicName
		{
			get { return String.IsNullOrEmpty(_publicName) ? _privateName : _publicName; }
			set { _publicName = value; }
		}

		/// <summary>
		/// Получить список записей настроек
		/// </summary>
		/// <value>Cписок записей настроек</value>
		public Dictionary<string, DbRecordData> Settings
		{
			get { return _settings; }
		}

		/// <summary>
		/// Получить список вложенных контейнеров настроек
		/// </summary>
		/// <value>Cписок вложенных контейнеров настроек</value>
		public Dictionary<string, SettingsContainer> Groups
		{
			get { return _groups; }
		}

		/// <summary>
		/// Получить или задать ссылку на тип графического элемента связанного с контейнером
		/// </summary>
		/// <value>
		/// Ссылка на тип графического элемента связанного с контейнером.
		/// </value>
		public Type GraphicElement
		{
			get { return _graphicElement; }
			set { _graphicElement = value; }
		}

		public Guid Id
		{
			get { return _guid; }
		}

		/// <summary>
		/// Флаг отвечающий за динамическую десериализацию <see cref="DbRecordData"/>
		/// хранящихся в этом контейнере
		/// </summary>
		/// <value>
		/// 	<c>true</c> если будет использована динамическая дессериализация; иначе, <c>false</c>.
		/// </value>
		/// <remarks>
		/// Этот флаг необходим для корректного востановления контейнеров, которые являются аналогами 
		/// списков. Для таких контейнеров невозможно заранее определить количество элементов. Поэтому остается
		/// загружать их динамически
		/// </remarks>
		public bool IsDynamic
		{
			get { return _isDynamic; }
			set { _isDynamic = value; }
		}

		/// <summary>
		/// Получить <see cref="Celsio.Data.DbRecordData"/> запись настроки по имени.
		/// </summary>
		public DbRecordData this[string name]
		{
			get { return _settings.ContainsKey(name) ? _settings[name] : null; }
		}

		/// <summary>
		/// Добавить настройку в контейнер
		/// </summary>
		/// <param name="setting">Добавляемая настройка</param>
		public void Add(DbRecordData setting)
		{
			_settings[setting.PrivateName] = setting;
		}

		public void Add(params DbRecordData[] settings)
		{
			foreach (DbRecordData setting in settings)
			{
				_settings[setting.PrivateName] = setting;
			}
		}

		/// <summary>
		/// Добавить вложенный контейнер
		/// </summary>
		/// <param name="group">Вложенный контейнер</param>
		public void Add(SettingsContainer group)
		{
			_groups.Add(group.PrivateName, group);
		}

		public SettingsContainer Clone()
		{
			SettingsContainer dst = new SettingsContainer();
			CloneImplementation(this, dst);
			return dst;
		}

		private void CloneImplementation(SettingsContainer src, SettingsContainer dst)
		{
			dst._privateName = string.Copy(src._privateName);
			dst._publicName = string.Copy(src._publicName);
			dst._graphicElement = src._graphicElement;
			dst._guid = src._guid;
			dst.IsDynamic = src.IsDynamic;

			foreach (KeyValuePair<string, DbRecordData> setting in src._settings)
			{
				string key = string.Copy(setting.Key);
				DbRecordData value = setting.Value.Copy();
				dst._settings.Add(key, value);
			}

			foreach (KeyValuePair<string, SettingsContainer> container in src._groups)
			{
				string key = string.Copy(container.Key);
				SettingsContainer value = container.Value.Clone();
				dst._groups.Add(key, value);
			}
		}
	}
}
