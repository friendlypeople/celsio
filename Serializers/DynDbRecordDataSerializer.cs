// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Xml;
using Celsio.Data;

namespace Celsio.Serializers
{
    public class DynDbRecordDataSerializer : IXmlSerializer<DbRecordData>
    {
        private static readonly ValueSection ValueSection = new ValueSection();
        private static readonly AttributeSection AttributeSection = new AttributeSection();

        public void Save(XmlWriter writer, DbRecordData data)
        {
            writer.WriteStartElement(data.PrivateName);
            writer.WriteAttributeString("dataType", data.Type.ToString());
            writer.WriteAttributeString("publicName", data.PublicName);

            ValueSection.SaveToXml(writer, data);
            AttributeSection.SaveToXml(writer, data);

            if (data.Items.Count > 0)
            {
                writer.WriteStartElement("items");
                foreach (DbRecordData item in data.Items)
                    Save(writer, item);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        public void Load(XmlNode node, DbRecordData data)
        {
            data.PrivateName = node.Name;
            if (node.Attributes == null)
                throw new Exception("Не верный формат данных в Xml: не найдены атрибуты");
            XmlAttribute pn = node.Attributes["publicName"];
            if (pn != null)
                data.PublicName = pn.Value;

            ValueSection.LoadFromXml(node, data);
            AttributeSection.LoadFromXml(node, data);

            XmlNode chNode = node.SelectSingleNode("items");
            if (chNode == null)
                return;
                //throw new Exception("Не верный формат данных в Xml: не найдены узел items");

			data.Items.Clear();
            foreach (XmlNode childNode in chNode.ChildNodes)
            {
                if (childNode.Attributes == null)
                    throw new Exception("Не верный формат данных в Xml: не найдены атрибуты");
                XmlAttribute dataTypeAttr = childNode.Attributes["dataType"];
				if (dataTypeAttr == null)
					throw new NullReferenceException("Отсутствует аттрибут: " + "'dataType' в узле: " + childNode.Name);
                DbRecordData childData = DbRecordData.Create(dataTypeAttr.Value);
                Load(childNode, childData);
                data.Items.Add(childData);
            }
        }
    }
}
