// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;

namespace Celsio.Serializers.Experemental.Core
{
	internal class TypeTable
	{
		private const string Signature = "tbl";

		private static readonly FastDualCache<Type, string> Cache = new FastDualCache<Type, string>();
		private static readonly FastDualCache<Type, string> WellKnownType = new FastDualCache<Type, string>();
		
		private readonly Dictionary<string, Type> _typeTable = new Dictionary<string, Type>();

		static TypeTable()
		{
			WellKnownType.Add(typeof(Int16), "Int16");
			WellKnownType.Add(typeof(Int32), "Int32");
			WellKnownType.Add(typeof(Int64), "Int64");

			WellKnownType.Add(typeof(UInt16), "UInt16");
			WellKnownType.Add(typeof(UInt32), "UInt32");
			WellKnownType.Add(typeof(UInt64), "UInt64");
		}

		private string Get(Type type)
		{
			if (Cache.ContainsFirst(type))
				return Cache.GetByFirst(type);
			string res = type.AssemblyQualifiedName;
			Cache.Add(type, res);
			return res;
		}

		private Type Get(string name)
		{
			if (Cache.ContainsSecond(name))
				return Cache.GetBySecond(name);

			Type t = Type.GetType(name);
			if (t == null) throw new Exception("Не удалось создать тип " + name);
			Cache.Add(t, name);
			return t;
		}

		public string RegisterType(Type type)
		{
			if (_typeTable.ContainsValue(type))
			{
				foreach (KeyValuePair<string, Type> kvp in _typeTable)
				{
					if (kvp.Value == type)
						return kvp.Key;
				}
			}
			string tableMarker = Signature + _typeTable.Count;
			_typeTable.Add(tableMarker, type);
			return tableMarker;
		}

		public Type ExtractType(string tableMarker)
		{
			if (_typeTable.ContainsKey(tableMarker))
				return _typeTable[tableMarker];
			return null;
		}
	}
}
