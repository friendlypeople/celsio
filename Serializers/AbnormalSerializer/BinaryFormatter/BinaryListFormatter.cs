// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.IO;
using Celsio.Commons.Utils;
using Celsio.Serializers.AbnormalSerializer.Support;

namespace Celsio.Serializers.AbnormalSerializer.BinaryFormatter
{
	class BinaryListFormatter : IBinaryFormatter
	{
		public virtual void WriteElement(Stream stream, object obj, bool needJump)
		{
            if (obj == null)
            {
                Halt(stream, needJump);
                return;
            }
		    IList list = (IList)obj;
            if (list.Count > 0)
            {
                IBinaryFormatter iciw = null;

                if (needJump)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        BinaryAccessMechanism bacm = new BinaryAccessMechanism(ms);
                        bacm.Write<int>(list.Count);
                        foreach (object elem in list)
                        {
                            if (elem == null)
                                bacm.Write<string>(typeof(Nullable));
                            else
                            {
                                if (iciw == null)
                                    iciw = BinaryFormatterFactory.CreateProduct(SerializationBase.CheckType(elem));                                   
                                bacm.Write<String>(TypeTable.Get(elem.GetType()));
                                iciw.WriteElement(ms, elem, false);
                            }
                        }
                        long writeSize = ms.Length;
                        bacm.SelectStream(stream);
                        bacm.Write<long>(writeSize);
                        bacm.Write(ms);
                    }
                }
                else
                {
                    BinaryAccessMechanism bacm = new BinaryAccessMechanism(stream);
                    bacm.Write<int>(list.Count);
                    foreach (object elem in list)
                    {
                        if (elem == null)
                            bacm.Write<string>(typeof(Nullable));
                        else
                        {
                            if (iciw == null)
                                iciw = BinaryFormatterFactory.CreateProduct(SerializationBase.CheckType(elem));
                            bacm.Write<String>(TypeTable.Get(elem.GetType()));
                            iciw.WriteElement(stream, elem, false);
                        }
                    }
                }
            }
            else
                Halt(stream, needJump);
		}

        private void Halt(Stream stream, bool needJump)
        {
            BinaryAccessMechanism bacm = new BinaryAccessMechanism(stream);
            if (needJump)
            {
                //������ ���� ������ ����, ����� �� �������� ����������.
                const long writeSize = 0;
                bacm.Write<long>(writeSize);
            }
            bacm.Write<int>(0);
        }

		public virtual bool ReadElement(Stream stream, ref object obj, Type objectType, bool needJump)
		{
			obj = SerializationBase.RecreateObject(objectType);
			// ���������� �������� ����
			IBinaryFormatter deserializer = null;
			BinaryAccessMechanism bacm = new BinaryAccessMechanism(stream);
			
			// ������ ���������� ���������� ���� ����������� � ������ ����
			if (needJump) bacm.Read<long>();

			int count = bacm.Read<int>();
			for (int i = 0; i < count; i++)
			{
                object iobj = null;
				string tstr = bacm.Read<String>();
				Type type = TypeTable.Get(tstr);
                
                if (type == typeof(Nullable))
                    ((IList)obj).Add(null);
                else
                {
                    if (deserializer == null)
                        deserializer = BinaryFormatterFactory.CreateProduct(SerializationBase.CheckType(type));
                    if (deserializer.ReadElement(stream, ref iobj, type, false))
                        ((IList) obj).Add(iobj);
                    else
                        return false;
                }
			}
			return true;
		}
	}
}
