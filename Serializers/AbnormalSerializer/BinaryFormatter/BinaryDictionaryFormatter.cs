// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.IO;
using Celsio.Commons.Utils;
using Celsio.Serializers.AbnormalSerializer.Support;

namespace Celsio.Serializers.AbnormalSerializer.BinaryFormatter
{
	class BinaryDictionaryFormatter : IBinaryFormatter
	{
		public void WriteElement(Stream stream, object obj, bool needJump)
		{
            if (obj == null)
            {
                Halt(stream, needJump);
                return;
            }
		    IDictionary dict = (IDictionary)obj;


		    if (dict.Count > 0)
		    {
		        IDictionaryEnumerator denum = dict.GetEnumerator();
                if (!denum.MoveNext())
                {
                    Halt(stream, needJump);
                    return;
                }

		        IBinaryFormatter kiciw = BinaryFormatterFactory.CreateProduct(SerializationBase.CheckType(denum.Key));
		        IBinaryFormatter viciw = BinaryFormatterFactory.CreateProduct(SerializationBase.CheckType(denum.Value));

		        if (needJump)
		        {
		            using (MemoryStream ms = new MemoryStream())
		            {
		                BinaryAccessMechanism bacm = new BinaryAccessMechanism(ms);
		                bacm.Write<int>(dict.Count);
		                // ��������� �����
		                do
		                {
		                    bacm.Write<String>(TypeTable.Get(denum.Key.GetType()));
		                    kiciw.WriteElement(ms, denum.Key, false);
		                    bacm.Write<String>(TypeTable.Get(denum.Value.GetType()));
		                    viciw.WriteElement(ms, denum.Value, false);
		                } while (denum.MoveNext());

		                long writeSize = ms.Length;
		                bacm.SelectStream(stream);
		                bacm.Write<long>(writeSize);
		                bacm.Write(ms);
		            }
		        }
		        else
		        {
		            BinaryAccessMechanism bacm = new BinaryAccessMechanism(stream);
		            bacm.Write<int>(dict.Count);
		            // ��������� �����
		            do
		            {
		                bacm.Write<String>(TypeTable.Get(denum.Key.GetType()));
		                kiciw.WriteElement(stream, denum.Key, false);
		                bacm.Write<String>(TypeTable.Get(denum.Value.GetType()));
		                viciw.WriteElement(stream, denum.Value, false);
		            } while (denum.MoveNext());
		        }
		    }
		    else
		        Halt(stream, needJump);
		}

        private void Halt(Stream stream, bool needJump)
        {
            BinaryAccessMechanism bacm = new BinaryAccessMechanism(stream);
            if (needJump)
            {
                //������ ���� ������ ����, ����� �� �������� ����������.
                const long writeSize = 0;
                bacm.Write<long>(writeSize);
            }
            bacm.Write<int>(0);
        }

	    public bool ReadElement(Stream stream, ref object obj, Type objectType, bool needJump)
		{
			obj = SerializationBase.RecreateObject(objectType);
			// ���� �� ����� ���� / ��������
			IBinaryFormatter kdes = null;
			IBinaryFormatter vdes = null;

			BinaryAccessMechanism bacm = new BinaryAccessMechanism(stream);
			// ������ ���������� ���������� ���� ����������� � ������ ����
			if (needJump) bacm.Read<long>();
			int count = bacm.Read<int>();

			for (int i = 0; i < count; i++)
			{
				string tstrK = bacm.Read<String>();
				Type ktype = TypeTable.Get(tstrK);
				object kobj = null;
				if (kdes == null) kdes = BinaryFormatterFactory.CreateProduct(SerializationBase.CheckType(ktype));

				if (kdes.ReadElement(stream, ref kobj, ktype, false))
				{
					string tstrV = bacm.Read<String>();
					Type vtype = TypeTable.Get(tstrV);
					object vobj = null;
					if (vdes == null) vdes = BinaryFormatterFactory.CreateProduct(SerializationBase.CheckType(vtype));

					if (vdes.ReadElement(stream, ref vobj, vtype, false))
					{
						((IDictionary)obj).Add(kobj, vobj);
					}
					else return false;
				}
				else return false;
			}
			return true;
		}
	}
}
