// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Xml;
using Celsio.Serializers.AbnormalSerializer.Support;

namespace Celsio.Serializers.AbnormalSerializer
{
	/// <summary>
	/// ����� ������ ������� �� Xml
	/// </summary>
	class XmlDeserializator : SerializationBase, IXmlDeserialization
	{
		public XmlDeserializator(Type type) : base (type) { }

		private void InternalDeserialize(XmlNode node, object obj, TypeTable table)
		{
			if (table != null) table.ExportXml(node);

			foreach (XmlNode chn in node)
			{
				FieldInfoData fid = _fidTable.Get(chn.Name);
				if (fid == null) continue;
				object sobj = fid.FieldInfo.GetValue(obj);
				if (fid.XmlFormatter.ReadElement(chn, ref sobj, fid.FieldInfo.FieldType, table))
				{
					fid.FieldInfo.SetValue(obj, sobj);
				}
			}
		}

		public void Deserialize(XmlNode node, object obj)
		{
			if (node == null)
			{
				System.Diagnostics.Debug.Assert(false, "XmlNode node == null");
				return;
			}			
			
			TypeTable table = null;
			if (_mainAttribute.UseTypeTable)
				table = new TypeTable();

			InternalDeserialize(node, obj, table);

		}

		public void Deserialize(XmlReader reader, object obj)
		{
			try
			{
				if (reader == null)
				{
					System.Diagnostics.Debug.Assert(false, "XmlReader reader == null");
					return;
				}
				XmlDocument doc = new XmlDocument();
				// �������� �������� �� ������ ���������� ������������� �����
				// ������ ��� ���� ��� ��������� ������
				XmlReader temp = reader.ReadSubtree();
				temp.Read();
				doc.Load(temp);
				
				TypeTable table = null;
				if (_mainAttribute.UseTypeTable) table = new TypeTable();

				InternalDeserialize(doc.DocumentElement, obj, table);
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.Assert(false, "Abnormal Desirializer",
				                                ex.ToString());
			}
		}
	}
}
