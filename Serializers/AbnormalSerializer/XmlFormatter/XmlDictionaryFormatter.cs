// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Xml;
using Celsio.Serializers.AbnormalSerializer.Support;

namespace Celsio.Serializers.AbnormalSerializer.XmlFormatter
{
	class XmlDictionaryFormatter : IXmlFormatter
	{
		/// <summary>
		/// ������ �������� �� Xml
		/// </summary>
		/// <param name="node">���� Xml ���������</param>
		/// <param name="obj">�������� ����</param>
		/// <param name="table">������� ����� ��� ��������</param>
		public bool ReadElement(XmlNode node, ref object obj, Type objectType, TypeTable table)
		{
			//if (obj == null) return false;
			//((IDictionary)obj).Clear();		
			obj = SerializationBase.RecreateObject(objectType);
			// ���� �� ����� ���� / ��������
			IXmlFormatter kdes = null;
			IXmlFormatter vdes = null;
			for (int i = 0; i < node.ChildNodes.Count; i += 2)
			{
				XmlNode keyNode = node.ChildNodes[i];
				XmlNode valNode = node.ChildNodes[i + 1];

				Type ktype = SerializationBase.ReadTypeAttribute(keyNode, table);
				Type vtype = SerializationBase.ReadTypeAttribute(valNode, table);
				if (ktype == null || vtype == null) return false;

				object kobj = null;//SerializationBase.RecreateObject(ktype);
				object vobj = null;// SerializationBase.RecreateObject(vtype);

				if (kdes == null)
					kdes = XmlFormatterFactory.CreateProduct(SerializationBase.CheckType(ktype));
				if (vdes == null)
					vdes = XmlFormatterFactory.CreateProduct(SerializationBase.CheckType(vtype));

				if (kdes.ReadElement(keyNode, ref kobj, ktype, table))
				{
					if (vdes.ReadElement(valNode, ref vobj, vtype, table))
					{
						((IDictionary)obj).Add(kobj, vobj);
					}
					else return false;
				}
				else return false;
			}
			return true;
		}

		/// <summary>
		/// ������ �������� � Xml
		/// </summary>
		/// <param name="wr">������ �������� (���� ��������� ���� ����)</param>
		/// <param name="obj">�������� ����</param>
		/// <param name="table">������� ����� ��� ��������</param>
		public void WriteElement(XmlWriter wr, object obj, TypeTable table)
		{
			if (obj == null) return;
			IDictionary dict = (IDictionary)obj;
			if (dict.Count > 0)
			{
				IDictionaryEnumerator denum = dict.GetEnumerator();
				if (!denum.MoveNext()) return;

				IXmlFormatter kiciw = XmlFormatterFactory.CreateProduct(SerializationBase.CheckType(denum.Key));
				IXmlFormatter viciw = XmlFormatterFactory.CreateProduct(SerializationBase.CheckType(denum.Value));
				if (kiciw == null || viciw == null) return;
				SerializationBase.WriteTypeAttribute(wr, obj.GetType(), table);

				// ��������� �����
				do
				{
					wr.WriteStartElement("key");
					kiciw.WriteElement(wr, denum.Key, table);
					wr.WriteEndElement();

					wr.WriteStartElement("val");
					viciw.WriteElement(wr, denum.Value, table);
					wr.WriteEndElement();

				} while (denum.MoveNext());
			}
		}
	}
}
