// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using Celsio.Commons.Utils;
using Celsio.Runtime.Converters;
using Celsio.Serializers.AbnormalSerializer.Support;

namespace Celsio.Serializers.AbnormalSerializer
{
	/// <summary>
	/// ������� ����� ��� ������� ������������
	/// </summary>
	internal abstract class SerializationBase
	{
		protected readonly Type _type;
		protected readonly string _mainName;
		protected const string TypeAttribute = "t";
		protected readonly FieldTable _fidTable = new FieldTable();
		protected readonly AbnormalSerializeAttribute _mainAttribute;

		private static readonly Dictionary<Type, TypeConverter>			ConverterCache = new Dictionary<Type, TypeConverter>(10);
		private static readonly Dictionary<Type, DeclaratorType>		TypeCache = new Dictionary<Type, DeclaratorType>(10);
		private static readonly Dictionary<Type, ObjectCreateMethod>	CreatorCache = new Dictionary<Type, ObjectCreateMethod>(10);

		public static BinaryAccessMechanism BinaryAm = new BinaryAccessMechanism(null);


		protected SerializationBase(Type type)
		{ 
			_type = type;
			
			_mainAttribute = (AbnormalSerializeAttribute)
			                                  Attribute.GetCustomAttribute(_type, typeof(AbnormalSerializeAttribute));

			if (_mainAttribute == null)
				throw new Exception("�� ������ ������� AbnormalSerializeAttribute");
			_fidTable.InitializeTable(type);

			_mainName = _type.Name;
			if (!String.IsNullOrEmpty(_mainAttribute.Name))
				_mainName = _mainAttribute.Name;
		}

		
		public static TypeConverter GetSerializeConverter(Type type)
		{
			if (ConverterCache.ContainsKey(type))
				return ConverterCache[type];
			TypeConverter tc = TypeConverterProvider.GetSpecificConverter(type);
			ConverterCache.Add(type, tc);
			return tc;

		}

		public static void WriteTypeAttribute(XmlWriter wr, Type type, TypeTable table)
		{
			if (table == null)
				wr.WriteAttributeString(TypeAttribute, TypeTable.Get(type));
			else
			{
				string str = table.RegisterType(type);
				wr.WriteAttributeString(TypeAttribute, str);
			}
		}

		public static Type ReadTypeAttribute(XmlNode node, TypeTable table)
		{
			XmlAttribute xatr = node.Attributes[TypeAttribute];
			if (xatr == null) return null;
			// ���������� ���
			if (table == null)
				return TypeTable.Get(xatr.Value);
			return table.ExtractType(xatr.Value);
		}

		public static object RecreateObject(Type type)
		{
			//return Activator.CreateInstance(type);
			//if (!type.IsValueType)
			{
				if (CreatorCache.ContainsKey(type))
					return CreatorCache[type].CreateInstance();

				ConstructorInfo ci = type.GetConstructor(Type.EmptyTypes);
				System.Diagnostics.Debug.Assert(ci != null, "����������� ����������� �� ��������� ��� ���� " + type);
				if (ci == null) return null;
				ObjectCreateMethod mc = new ObjectCreateMethod(ci);
				CreatorCache.Add(type, mc);
				return mc.CreateInstance();
			}
			//return null;
		}

		public static object RecreateArrayObject(Type type, int arraySize)
		{
			if (CreatorCache.ContainsKey(type))
				return CreatorCache[type].CreateInstance();

			ObjectCreateMethod mc = new ObjectCreateMethod(type, arraySize);
			CreatorCache.Add(type, mc);
			return mc.CreateInstance();
		}

		public static DeclaratorType CheckType(Type type)
		{
			if (TypeCache.ContainsKey(type))
				return TypeCache[type];

			if (type.IsValueType) return DeclaratorType.Value;

            //if (type == typeof(Nullable))
            //{
            //    TypeCache.Add(type, DeclaratorType.Nullable);
            //    return DeclaratorType.Nullable;
            //}

		    if (type == typeof(String))
			{
				TypeCache.Add(type, DeclaratorType.String);
				return DeclaratorType.String;
			}
			if (type.IsSubclassOf(typeof(Array)))
			{
				TypeCache.Add(type, DeclaratorType.Array);
				return DeclaratorType.Array;
			}
			if (null != type.GetInterface(typeof(IList).Name))
			{
				TypeCache.Add(type, DeclaratorType.List);
				return DeclaratorType.List;
			}
			if (null != type.GetInterface(typeof(IDictionary).Name))
			{
				TypeCache.Add(type, DeclaratorType.Dictionary);
				return DeclaratorType.Dictionary;
			}

            if (type == typeof(object))
            {
                TypeCache.Add(type, DeclaratorType.Object);
                return DeclaratorType.Object;
            }

			if (type.IsClass || type.IsInterface)
			{
				TypeCache.Add(type, DeclaratorType.Class);
				return DeclaratorType.Class;
			}
			//if (null != type.GetInterface(typeof(IXmlSerializable).Name))
			//{
			//    TypeCache.Add(type, DeclaratorType.Class);
			//    return DeclaratorType.Class;
			//}

			//if (null != type.GetInterface(typeof(IBinarySerializable).Name))
			//{
			//    TypeCache.Add(type, DeclaratorType.Class);
			//    return DeclaratorType.Class;
			//}
			return DeclaratorType.Unknown;
		}

		public static DeclaratorType CheckType(object sobj)
		{
			if (sobj == null) return DeclaratorType.Value;
			if (sobj is String) return DeclaratorType.String;
			if (sobj is IXmlSerializable || sobj is IBinarySerializable) return DeclaratorType.Class;
			if (sobj is ICollection)
			{
				if (sobj is Array) return DeclaratorType.Array;
				if (sobj is IList) return DeclaratorType.List;
				if (sobj is IDictionary) return DeclaratorType.Dictionary;
			}
            if (sobj.GetType().IsClass) return DeclaratorType.Class;
			return DeclaratorType.Value;
		}
	}
}
