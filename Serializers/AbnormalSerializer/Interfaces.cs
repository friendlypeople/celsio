// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.IO;
using System.Xml;
using Celsio.Serializers.AbnormalSerializer.Support;

namespace Celsio.Serializers.AbnormalSerializer
{
	/// <summary>
	/// �������� Xml ���������������
	/// </summary>
	interface IXmlDeserialization
	{
		/// <summary>
		/// ������������ �������
		/// </summary>
		/// <param name="reader">������ �������� (������ ��������� ��� ������)</param>
		/// <param name="obj">������� ������ ��� ��������������</param>
		void Deserialize(XmlReader reader, object obj);
	}

	/// <summary>
	/// �������� ��������� ���������������
	/// </summary>
	interface IBinaryDeserialization
	{
		/// <summary>
		/// ������������ �������
		/// </summary>
		/// <param name="stream">����� �������� (������ ��������� ��� ������)</param>
		/// <param name="obj">������� ������ ��� ��������������</param>
		void Deserialize(Stream stream, object obj);
	}

	/// <summary>
	/// �������� Xml �������������
	/// </summary>
	interface IXmlSerialization
	{
		/// <summary>
		/// ������������ �������
		/// </summary>
		/// <param name="writer">������ �������� (���� ��������� ��� ������)</param>
		/// <param name="obj">������� ������ ��� ������������</param>
		void Serialize(XmlWriter writer, object obj);
	}

	/// <summary>
	/// �������� Xml �������������
	/// </summary>
	interface IBinarySerialization
	{
		/// <summary>
		/// ������������ �������
		/// </summary>
		/// <param name="stream">����� �������� (���� ��������� ��� ������)</param>
		/// <param name="obj">������� ������ ��� ������������</param>
		void Serialize(Stream stream, object obj);
	}



	/// <summary>
	/// ��������� ������ ����������� �� ������������ / �������������� Xml
	/// </summary>
	interface IXmlSerializer : IXmlSerialization, IXmlDeserialization { }

	/// <summary>
	/// ��������� ������ ����������� �� ������������ / �������������� ��������
	/// </summary>
	interface IBinarySerializer : IBinarySerialization, IBinaryDeserialization { }


	public interface IBinarySerializable
	{
		void WriteStream(Stream stream);
		void ReadStream(Stream stream);
	}


	interface IXmlFormatter
	{
		/// <summary>
		/// ������ �������� � Xml
		/// </summary>
		/// <param name="wr">������ �������� (���� ��������� ���� ����)</param>
		/// <param name="obj">�������� ����</param>
		/// <param name="table">������� ����� ��� ��������</param>
		void WriteElement(XmlWriter wr, object obj, TypeTable table);

		/// <summary>
		/// ������ �������� �� Xml
		/// </summary>
		/// <param name="node">���� Xml ���������</param>
		/// <param name="obj">�������� ����</param>
		/// <param name="objectType">��� �������</param>
		/// <param name="table">������� ����� ��� ��������</param>
		/// <returns></returns>
		bool ReadElement(XmlNode node, ref object obj, Type objectType, TypeTable table);
	}

	interface IBinaryFormatter
	{
		/// <summary>
		/// ������ �������� � Xml
		/// </summary>
		/// <param name="stream">����� �������� (���� ��������� ���� ����)</param>
		/// <param name="obj">�������� ����</param>
		/// <param name="needJump">��������� �� ������������� ���������� ����� ������������� ������ � �����.</param>
		/// <returns></returns>
		void WriteElement(Stream stream, object obj, bool needJump);

		/// <summary>
		/// ������ �������� �� Xml
		/// </summary>
		/// <param name="stream">����� �������� (������ ��������� ��� ������)</param>
		/// <param name="obj">�������� ����</param>
		/// <param name="objectType">��� �������</param>
		/// <param name="needJump">��������� �� ������������� ���������� ����� ������������� ������ � �����.</param>
		/// <returns></returns>
		bool ReadElement(Stream stream, ref object obj, Type objectType, bool needJump);
	}
}
