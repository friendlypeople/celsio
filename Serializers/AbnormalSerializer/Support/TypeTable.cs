// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Xml;

namespace Celsio.Serializers.AbnormalSerializer.Support
{
	public class TypeTableCash
	{
		private readonly Dictionary<Type, string> _signCache = new Dictionary<Type, string>(10);
		private readonly Dictionary<string, Type> _typeCache = new Dictionary<string, Type>(10);

		public void AddToCash(Type type, string name)
		{
			if (!_signCache.ContainsKey(type))
			{
				_signCache.Add(type, name);
				//if (!_typeCache.ContainsKey(name))
				_typeCache.Add(name, type);
			}
		}
		public string Get(Type type)
		{
			return _signCache[type];
		}
		public Type Get(string name)
		{
			return _typeCache[name];
		}
		public bool Contains(Type type)
		{
			return _signCache.ContainsKey(type);
		}
		public bool Contains(string str)
		{
			return _typeCache.ContainsKey(str);
		}
	}

	public class TypeTable
	{
		public const string XmlName	= "_tptbl_";
		private const string Signature	= "tbl";

		private static readonly TypeTableCash Ttc = new TypeTableCash();

		private readonly Dictionary<string, Type> _typeTable = new Dictionary<string, Type>();
		

		public static string Get(Type type)
		{
			if (Ttc.Contains(type))
				return Ttc.Get(type);
			//string res = TypeSignature.Get(type.AssemblyQualifiedName);
            string res = type.AssemblyQualifiedName;
			Ttc.AddToCash(type, res);
			return res;
		}

		public static Type Get(string name)
		{
			if (Ttc.Contains(name))
				return Ttc.Get(name);

			Type t = Type.GetType(name);
			if (t == null) throw new Exception("�� ������� ������� ��� " + name);
			Ttc.AddToCash(t, name);
			return t;
		}

		public string RegisterType(Type type)
		{
			if (_typeTable.ContainsValue(type))
			{
				foreach (KeyValuePair<string, Type> kvp in _typeTable)
				{
					if (kvp.Value == type)
						return kvp.Key;
				}
			}
			string tableMarker = Signature + _typeTable.Count;
			_typeTable.Add(tableMarker, type);
			return tableMarker;
		}

		public Type ExtractType(string tableMarker)
		{
			if (_typeTable.ContainsKey(tableMarker))
				return _typeTable[tableMarker];
			return null;
		}

		public void ExportXml(XmlNode node)
		{
			XmlNode childNode = node.SelectSingleNode(XmlName);
			if (childNode == null) return;
			foreach (XmlNode cn in childNode.ChildNodes)
			{
				Type t;
				string str = cn.InnerText;
				if (string.IsNullOrEmpty(str)) continue;
				if (Ttc.Contains(str)) 
					t = Ttc.Get(str);
				else
				{
					t = Type.GetType(str);
					Ttc.AddToCash(t, str);
				}
				if (t == null) throw new Exception("�� ������� ������� ��� " + str);
				_typeTable.Add(cn.Name, t);
			}
		}

		public void ImportXml(XmlWriter wr)
		{
			wr.WriteStartElement(XmlName);
			foreach (KeyValuePair<string, Type> kvp in _typeTable)
				wr.WriteElementString(kvp.Key, Get(kvp.Value));
			wr.WriteEndElement();
		}
	}
}
