// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Reflection;

namespace Celsio.Serializers.AbnormalSerializer.Support
{
	class FieldTable
	{
		private readonly List<FieldInfoData> _fidTable = new List<FieldInfoData>(10);
		private static readonly Dictionary<Type, List<FieldInfoData>> FieldCache = new Dictionary<Type, List<FieldInfoData>>(10);
		private readonly Dictionary<string, FieldInfoData> _nameTable = new Dictionary<string, FieldInfoData>(10);

		private DeclaratorType CheckType(SerializeDeclarateAttribute extType, Type type)
		{
			if (extType.Type == DeclaratorType.Unknown)
			{
				return SerializationBase.CheckType(type);
			}
			return extType.Type;
		}

		private void Add(FieldInfoData data)
		{
			_fidTable.Add(data);
			string name = GetName(data);
			_nameTable.Add(name, data);
		}

		private void Add(IEnumerable<FieldInfoData> ldata)
		{
			foreach (FieldInfoData data in ldata)
				Add(data);
		}

		/// <summary>
		///  ����������� ����� ���� Xml 
		/// </summary>
		public static string GetName(FieldInfoData data)
		{
			if (String.IsNullOrEmpty(data.Attribute.XmlName))
				return data.FieldInfo.Name;
			return data.Attribute.XmlName;
		}

		public void InitializeTable(Type type)
		{
			const BindingFlags bf = BindingFlags.NonPublic |
									BindingFlags.Public |
									BindingFlags.Instance |
									BindingFlags.DeclaredOnly;

			//List<FieldInfoData> listInfo = new List<FieldInfoData>(10);
			Type recurciveType = type;

			while (recurciveType != typeof(object))
			{
				if (FieldCache.ContainsKey(recurciveType))
				{
					Add(FieldCache[recurciveType]);
				}
				else
				{
					List<FieldInfoData> tempL = new List<FieldInfoData>(10);
					FieldInfo[] fis = recurciveType.GetFields(bf);
					if (fis.Length != 0)
					{
						foreach (FieldInfo fi in fis)
						{
							SerializeDeclarateAttribute xsat = (SerializeDeclarateAttribute)
														 Attribute.GetCustomAttribute(fi, typeof(SerializeDeclarateAttribute));
							if (xsat == null) continue;
							DeclaratorType xdtype = CheckType(xsat, fi.FieldType);

							IXmlFormatter xmlFormatter = XmlFormatterFactory.CreateProduct(xdtype);
							if (xmlFormatter == null)
								throw new Exception("�� ������� ����� ����� ��������� ��� ���� " + fi.FieldType.Name);

							IBinaryFormatter binaryFormatter = BinaryFormatterFactory.CreateProduct(xdtype);
							if (binaryFormatter == null)
								throw new Exception("�� ������� ����� ����� ��������� ��� ���� " + fi.FieldType.Name);

							FieldInfoData fid = new FieldInfoData(fi,xsat,xmlFormatter,binaryFormatter);
							Add(fid);
							tempL.Add(fid);
						}
					}
					FieldCache.Add(recurciveType, tempL);
				}
				recurciveType = recurciveType.BaseType;
				if (recurciveType == null) break;
			}
		}

		public FieldInfoData Get(string name)
		{
			if (String.IsNullOrEmpty(name))
				return null;
			if (_nameTable.ContainsKey(name))
				return _nameTable[name];
			return null;
		}

		public List<FieldInfoData> Items
		{
			get { return _fidTable; }
		}
	}
}
