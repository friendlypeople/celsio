// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;

namespace Celsio.Serializers.AbnormalSerializer
{
	/// <summary>
	/// ������� ������� �� ��������� ������ �������� ��
	/// ����������� ���������� �� ����� ��������������
	/// </summary>
	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
	public sealed class AbnormalSerializeAttribute : Attribute
	{
		private readonly string _name = String.Empty;
		private readonly bool _useTypeTable = true;
		
		
		public AbnormalSerializeAttribute() { }

		/// <summary>
		/// ������������� ������ ��������� <see cref="AbnormalSerializeAttribute"/> ������.
		/// </summary>
		/// <param name="name">��� ���� � ������ ����� �������� / ����������� ������</param>
		public AbnormalSerializeAttribute(string name)
		{
			_name = name;
		}

		public AbnormalSerializeAttribute(bool useTypeTable)
		{
			_useTypeTable = useTypeTable;
		}
		public AbnormalSerializeAttribute(string name, bool useTypeTable)
		{
			_name = name;
			_useTypeTable = useTypeTable;
		}


		public bool UseTypeTable
		{
			get { return _useTypeTable; }
		}

		public string Name
		{
			get { return _name; }
		}
	}

	[AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
	public sealed class SerializeDeclarateAttribute : Attribute
	{

		/// <summary>
		/// ��� �������� � Xml
		/// </summary>
		public string XmlName = string.Empty;
		public DeclaratorType Type = DeclaratorType.Unknown;

		/// <summary>
		/// ������������� ������ ��������� <see cref="SerializeDeclarateAttribute"/> ������.
		/// </summary>
		/// <param name="xmlName">��� ���� � ������ ����� ��������� / ������������ ����</param>
		/// <param name="type">��� ��������� ����</param>
		public SerializeDeclarateAttribute(string xmlName, DeclaratorType type)
		{
			XmlName = xmlName;
			Type = type;
		}

		/// <summary>
		/// ������������� ������ ��������� <see cref="SerializeDeclarateAttribute"/> ������.
		/// </summary>
		/// <param name="type">��� ��������� ����</param>
		public SerializeDeclarateAttribute(DeclaratorType type)
		{
			Type = type;
		}

		/// <summary>
		/// ������������� ������ ��������� <see cref="SerializeDeclarateAttribute"/> ������.
		/// </summary>
		/// <param name="xmlName">��� ���� � ������ ����� ��������� / ������������ ����</param>
		public SerializeDeclarateAttribute(string xmlName)
		{
			XmlName = xmlName;
		}

		/// <summary>
		/// ������������� ������ ��������� <see cref="SerializeDeclarateAttribute"/> ������.
		/// </summary>
		public SerializeDeclarateAttribute() { }
	}
}
