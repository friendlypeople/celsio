// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Xml;
using Celsio.Data;
using Celsio.Runtime.Converters;

namespace Celsio.Serializers
{
    public class DbRecordDataSerializer : IXmlSerializer<DbRecordData>
	{
		private static readonly ValueSection ValueSection = new ValueSection();
		private static readonly AttributeSection AttributeSection = new AttributeSection();

		public void Save(XmlWriter writer, DbRecordData data)
		{
			writer.WriteStartElement(data.PrivateName);
			writer.WriteAttributeString("dataType", data.Type.ToString());
			writer.WriteAttributeString("publicName", data.PublicName);

			ValueSection.SaveToXml(writer, data);
			AttributeSection.SaveToXml(writer, data);

            if (data.Items.Count > 0)
            {
                writer.WriteStartElement("items");
                foreach (DbRecordData item in data.Items)
                    Save(writer, item);
                writer.WriteEndElement();
            }

		    writer.WriteEndElement();
		}

		public void Load(XmlNode node, DbRecordData data)
		{
			if (node.Attributes == null)
				throw new Exception("Не верный формат данных в Xml: не найдены атрибуты");
			XmlAttribute pn = node.Attributes["publicName"];
            if (pn != null)
			    data.PublicName = pn.Value;

			ValueSection.LoadFromXml(node, data);
			AttributeSection.LoadFromXml(node, data);

			XmlNode chNode = node.SelectSingleNode("items");
            if (chNode == null)
                return;
				//throw new Exception("Не верный формат данных в Xml: не найдены узел items");

			foreach (XmlNode childNode in chNode.ChildNodes)
			{
				foreach (DbRecordData item in data.Items)
				{
					if (item.PrivateName == childNode.Name)
					{
						Load(childNode, item);
						break;
					}
				}
			}
		}
	}

	internal sealed class ValueSection
	{
		public void SaveToXml(XmlWriter writer, DbRecordData data)
		{
            string value = data.Type != DbDataType.Blob ?
                TypeConverterProvider.SpecificConvertToString(data.SystemType, data.Value) :
                Convert.ToBase64String((byte[])data.Value);

            if (string.IsNullOrEmpty(value))
                return;
            writer.WriteStartElement("value");
            writer.WriteValue(value);
			writer.WriteEndElement();
		}

		public void LoadFromXml(XmlNode node, DbRecordData data)
		{
			XmlNode chNode = node.SelectSingleNode("value");
            if (chNode == null)
                //throw new Exception("Не верный формат данных в Xml: не найдены узел value");
                return;

			if (data.Type != DbDataType.Blob)
				data.Value = TypeConverterProvider.SpecificConvertFromString(data.SystemType, chNode.InnerText);
			else
				data.Value = Convert.FromBase64String(chNode.InnerText);
		}
	}

	internal sealed class AttributeSection
	{
		public void SaveToXml(XmlWriter writer, DbRecordData data)
		{
            if (data.Attributes.Count == 0)
                return;
			writer.WriteStartElement("attributes");
			foreach (Pair<string, string> attribute in data.Attributes)
			{
				writer.WriteStartElement(attribute.First);
				writer.WriteValue(attribute.Second);
				writer.WriteEndElement();
			}
			writer.WriteEndElement();
		}

		public void LoadFromXml(XmlNode node, DbRecordData data)
		{
			XmlNode chNode = node.SelectSingleNode("atrributes");
			if (chNode == null)
			{
				chNode = node.SelectSingleNode("attributes");
				if (chNode == null)
					return;
			}
			data.Attributes.Clear();
			foreach (XmlNode childNode in chNode.ChildNodes)
			{
                data.Attributes.Add(childNode.Name, childNode.InnerText);
			}
		}
	}

}
