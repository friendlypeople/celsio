// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Celsio.Patterns
{
    public class ModuleRecord<T> where T : class
    {
        private readonly string _path = String.Empty;
        private readonly T _plugin;

        public string Path { get { return _path; } }
        public T Module { get { return _plugin; } }

        public ModuleRecord(string pth, T plgn)
        {
            _path = pth;
            _plugin = plgn;
        }
    }

    /// <summary>
    /// ����� �������������� �������� ��������
    /// </summary>
    /// <typeparam name="T">��������� ������� ����� �������� �� ��������</typeparam>
    public class ModuleLoader<T> where T : class
    {
        private readonly List<ModuleRecord<T>> _lPlugins = new List<ModuleRecord<T>>(10);

        public List<ModuleRecord<T>> Items { get { return _lPlugins; } }

        public int Count { get { return _lPlugins.Count; } }

        public void Load(List<string> paths)
        {
            foreach (string file in paths)
            {
                if (File.Exists(file))
                    LoadModule(file);
            }
        }

        private void LoadModule(string file)
        {
            try
            {
                Assembly assembly = Assembly.LoadFrom(file);

                foreach (Type type in assembly.GetTypes())
                {
                    Type iface = type.GetInterface(typeof(T).Name);
                    if (iface != null)
                    {
                        T module = Activator.CreateInstance(type) as T;
                        if (module != null)
                        {
                            _lPlugins.Add(new ModuleRecord<T>(file, module));
                        }
                        //else
                        //    EditorRoutine.WriteMessage("������ �������� ������: " + file);
                        break;
                    }
                }
            }
            catch (ReflectionTypeLoadException ex)
            {
                throw;
                //foreach (Exception e in ex.LoaderExceptions)
                //{
                //    EditorRoutine.WriteMessage("-------------------------------------");
                //    EditorRoutine.WriteMessage(e.ToString());
                //    EditorRoutine.WriteMessage("-------------------------------------");
                //}
            }
            catch (Exception ex)
            {
                throw;
                //EditorRoutine.WriteMessage(ex.ToString());
            }
        }

        //private string[] PreloadAssembly(string path)
        //{
        //    string[] files = Directory.GetFiles(path, "*.dll");
        //    List<string> lpl = new List<string>(5);
        //    // ���������� ����������� ��� ����������� ������ � ������
        //    AppDomainSetup ap = new AppDomainSetup();
        //    AppDomain preLoadDomain = AppDomain.CreateDomain("splitPreloadDomain", null, ap);

        //    foreach (string file in files)
        //    {
        //        try
        //        {
        //            Assembly ass = preLoadDomain.Load(file);
        //            foreach (Type type in ass.GetTypes())
        //            {
        //                Type iface = type.GetInterface(typeof(T).Name);
        //                if (iface != null)
        //                {
        //                    lpl.Add(file);
        //                    break;
        //                }
        //            }
        //        }
        //        catch (ReflectionTypeLoadException ex)
        //        {
        //            foreach (Exception e in ex.LoaderExceptions)
        //            {
        //                EditorRoutine.WriteMessage("-------------------------------------");
        //                EditorRoutine.WriteMessage(e.ToString());
        //                EditorRoutine.WriteMessage("-------------------------------------");
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            EditorRoutine.WriteMessage(ex.ToString());
        //        }
        //    }
        //    AppDomain.Unload(preLoadDomain);
        //    return lpl.ToArray();
        //}

        //public void Load(string searchpath)
        //{
        //    // �������� �� searchpath
        //    if (!Directory.Exists(searchpath))
        //    {
        //        EditorRoutine.WriteMessage("�� ������� ����� � ������������� ��������:");
        //        EditorRoutine.WriteMessage(searchpath);
        //        return;
        //    }
        //    //Directory.GetDirectories(searchpath);
        //    //string[] files = PreloadAssembly(searchpath);
        //    string[] files = Directory.GetFiles(searchpath, "*.dll");


        //    //������ � ��� ����� ������ �������
        //    foreach (string file in files)
        //    {
        //        LoadModule(file);
        //    }
        //    DumpDomenInfo(AppDomain.CurrentDomain);
        //}

        //private void DumpDomenInfo(AppDomain appDomain)
        //{
        //    EditorRoutine.WriteMessage("������ ����������:" + appDomain.BaseDirectory);
        //    EditorRoutine.WriteMessage("��������� ����:" + appDomain.RelativeSearchPath);
        //    EditorRoutine.WriteMessage("����������� ������: ");
        //    foreach (Assembly ass in appDomain.GetAssemblies())
        //    {
        //        EditorRoutine.WriteMessage(ass.GetName().Name);
        //    }

        //}
    }
}
