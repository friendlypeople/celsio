// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Celsio.Commons.Utils;

namespace Celsio.Patterns.DynamicProxy
{
	internal class StubProxyBodyWriter : IStubBodyWriter
	{
		private readonly Dictionary<string, FieldBuilder> _fieldBuilderMap = new Dictionary<string, FieldBuilder>();
		
		private ParameterInfo[] _pars;


		public StubProxyBodyWriter(Dictionary<string, FieldBuilder> fieldBuilderMap)
		{
			_fieldBuilderMap = fieldBuilderMap;
		}

		// Local variables
		// [0] Type[]		- список типов для определния сигнатуры метода (GetMethod)
		// [1] MethodInfo	- метод для вызова в заглушке
		// [2] Type[]		- список типов для генерик метода (U, V, T, R и т.д.)
		// [3] object[]		- список параметров для вызова метода (Invoke)
		// [4] RetVal		- возвращяемое значение (Тип определятся на момент il генерации)
		private int WriteLocalVariable(ILGenerator gen, MethodInfo mi)
		{
			gen.DeclareLocal(typeof(Type[]));
			gen.DeclareLocal(typeof(MethodInfo));
			gen.DeclareLocal(typeof(Type[]));

			int offset = 2;

			if (_pars.Length > 0)
			{
				gen.DeclareLocal(typeof(object[]));
				offset++;
			}

			if (mi.ReturnType != typeof(void))
			{
				gen.DeclareLocal(mi.ReturnType);
				offset++;
			}
			return offset;
		}

		private void WriteCreateArrayInLocalVariable(ILGenerator gen, Type arType, int length, int numInStack)
		{
			gen.Emit(OpCodes.Ldc_I4, length);
			gen.Emit(OpCodes.Newarr, arType);
			gen.Emit(OpCodes.Stloc, numInStack);
		}

		private void WriteTypeArrayToLocalVariable(ILGenerator gen, Type[] types, int numInStack)
		{
			Type[] args = new[] { typeof(RuntimeTypeHandle) };

			MethodInfo getTypeFromHandle = typeof(Type).GetMethod("GetTypeFromHandle", args);

			
			for (int i = 0; i < types.Length; i++)
			{
				gen.Emit(OpCodes.Ldloc, numInStack);
				gen.Emit(OpCodes.Ldc_I4, i);

				if (types[i].IsByRef)
				{
					Type tType = types[i].GetElementType();
					gen.Emit(OpCodes.Ldtoken, tType);

					// Type::GetTypeFromHandle(Type[])
					gen.EmitCall(OpCodes.Call, getTypeFromHandle, args);

					// Type::MakeByRefType()
					gen.Emit(OpCodes.Callvirt, typeof(Type).GetMethod("MakeByRefType"));
				}
				else
				{
					gen.Emit(OpCodes.Ldtoken, types[i]);

					// Type::GetTypeFromHandle(Type[])
					gen.EmitCall(OpCodes.Call, getTypeFromHandle, args);
				}
				gen.Emit(OpCodes.Stelem_Ref);
			}
		}

		// Оставлено для возможной отладки
		private void WriteArrayConsoleOutput(ILGenerator gen, int i, int len)
		{
			Type[] args = new[] { typeof(object) };

			gen.Emit(OpCodes.Ldstr, "--------------------------------");
			gen.EmitCall(OpCodes.Call, typeof(Console).GetMethod("WriteLine", args), args);
			gen.Emit(OpCodes.Ldloc, i);
			gen.Emit(OpCodes.Ldlen);
			gen.Emit(OpCodes.Conv_I4);

			gen.EmitCall(OpCodes.Call, typeof(Console).GetMethod("WriteLine", new[] { typeof(int) }), new[] { typeof(int) });


			for (int j = 0; j < len; j++)
			{
				gen.Emit(OpCodes.Ldloc, i);
				gen.Emit(OpCodes.Ldc_I4, j);
				gen.Emit(OpCodes.Ldelem, typeof(object));
				gen.EmitCall(OpCodes.Call, typeof(Console).GetMethod("WriteLine", args), args);
			}
			gen.Emit(OpCodes.Ldstr, "--------------------------------");
			gen.EmitCall(OpCodes.Call, typeof(Console).GetMethod("WriteLine", args), args);

		}

		// Оставлено для возможной отладки
		private void WriteObjectConsoleOutput(ILGenerator gen, int i)
		{
			Type[] args = new[] { typeof(object) };

			gen.Emit(OpCodes.Ldstr, "ToString(): ");
			gen.EmitCall(OpCodes.Call, typeof(Console).GetMethod("Write", args), args);
			gen.Emit(OpCodes.Ldloc, i);
			gen.EmitCall(OpCodes.Call, typeof(Console).GetMethod("WriteLine", args), args);
		}

		public void Write(ILGenerator gen, MethodInfo mi)
		{
			_pars = mi.GetParameters();
			int offset = WriteLocalVariable(gen, mi);

            gen.Emit(OpCodes.Ldarg, 0);
            gen.Emit(OpCodes.Ldfld, _fieldBuilderMap[FieldNames.MethodsDictionaryName]);
			gen.Emit(OpCodes.Ldstr, ReflectionSupport.GetSignature(mi));
            gen.Emit(OpCodes.Callvirt, typeof(Dictionary<string, MethodInfo>).GetMethod("get_Item", new[] { typeof(string) }));
            gen.Emit(OpCodes.Stloc, 1);

			//WriteObjectConsoleOutput(gen, 1);

			// эмитим генерик параметры и формируем генерик метод
			if (mi.IsGenericMethod)
				WriteGetGenericMethodInfo(gen, mi);

			if (_pars.Length > 0)
			{
				WriteCreateArrayInLocalVariable(gen, typeof(Object), _pars.Length, 3);
				gen.Emit(OpCodes.Ldloc, 3);

				for (int i = 0; i < _pars.Length; i++)
				{
					ParameterInfo p = _pars[i];
					Type type = p.ParameterType.IsByRef ? p.ParameterType.GetElementType() : p.ParameterType;

					if (p.IsOut) continue;

					gen.Emit(OpCodes.Ldc_I4, i);
					gen.Emit(OpCodes.Ldarg, i + 1);

					if (p.ParameterType.IsByRef)
						gen.Emit(OpCodes.Ldind_Ref);

					if (type.IsValueType || type.IsGenericParameter)
						gen.Emit(OpCodes.Box, type);

					gen.Emit(OpCodes.Stelem_Ref);
					gen.Emit(OpCodes.Ldloc, 3);
				}

				gen.Emit(OpCodes.Stloc, 3);
				gen.Emit(OpCodes.Ldloc, 1);
				gen.Emit(OpCodes.Ldarg, 0);
				gen.Emit(OpCodes.Ldfld, _fieldBuilderMap[FieldNames.InstanceName]);
				gen.Emit(OpCodes.Ldloc, 3);
			}
			else
			{
				gen.Emit(OpCodes.Ldloc, 1);
				gen.Emit(OpCodes.Ldarg, 0);
				gen.Emit(OpCodes.Ldfld, _fieldBuilderMap[FieldNames.InstanceName]);
				gen.Emit(OpCodes.Ldnull);
			}

			Type[] virtCallArgs = new[] { typeof(object), typeof(object[]) };

			// MethodBase::GetMethod(object, object[])
			gen.EmitCall(OpCodes.Callvirt, typeof(MethodInfo).GetMethod("Invoke", virtCallArgs), virtCallArgs);

			WriteReturnValue(gen, mi, offset);
			WriteRefOutResponce(gen, _pars);

			gen.Emit(OpCodes.Ret);
		}

		private void WriteGetGenericMethodInfo(ILGenerator gen, MethodInfo mi)
		{
			Type[] virtCallArgs;
			Type[] arguments = mi.GetGenericArguments();

			WriteCreateArrayInLocalVariable(gen, typeof (Type), arguments.Length, 2);
			WriteTypeArrayToLocalVariable(gen, arguments, 2);

			gen.Emit(OpCodes.Ldloc, 1);
			gen.Emit(OpCodes.Ldloc, 2);
			virtCallArgs = new[] {typeof (Type[])};
			// MethodInfo::GetMethod(string, Type[])
			gen.EmitCall(OpCodes.Callvirt, typeof (MethodInfo).GetMethod("MakeGenericMethod", virtCallArgs), virtCallArgs);
			gen.Emit(OpCodes.Stloc, 1);
			//WriteObjectConsoleOutput(gen, 1);
		}


		private void WriteReturnValue(ILGenerator gen, MethodInfo mi, int offset)
		{
			Type returnType = mi.ReturnType;
			if (returnType != typeof(void))
			{
				if (returnType.IsValueType || returnType.IsGenericParameter)
					gen.Emit(OpCodes.Unbox_Any, returnType);

				gen.Emit(OpCodes.Stloc, offset);
				gen.Emit(OpCodes.Ldloc, offset);
			}
			else
				gen.Emit(OpCodes.Pop);
		}

		// Возвращаем ref / out через прокси
		private void WriteRefOutResponce(ILGenerator gen, ParameterInfo[] pars)
		{
			for (int i = 0; i < pars.Length; i++)
			{
				Type parameterType = pars[i].ParameterType;
				if (!parameterType.IsByRef) continue;

				gen.Emit(OpCodes.Ldarg, i + 1);
				gen.Emit(OpCodes.Ldloc, 3);
				gen.Emit(OpCodes.Ldc_I4, i);
				gen.Emit(OpCodes.Ldelem, typeof(object));
				Type type = parameterType.GetElementType();
				if (type.IsValueType)
					gen.Emit(OpCodes.Unbox_Any, type);
				else
					gen.Emit(OpCodes.Castclass, type);
				gen.Emit(OpCodes.Stind_Ref);
			}
		}
	}
}
