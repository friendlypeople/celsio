// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Celsio.Commons.Utils;

namespace Celsio.Patterns.DynamicProxy
{
	internal sealed class ILProxyGenerator
	{
		const MethodAttributes MethAttribute = MethodAttributes.Public | 
			MethodAttributes.Virtual |
			MethodAttributes.HideBySig;

		const MethodAttributes PropAttribute = MethAttribute | MethodAttributes.SpecialName;


		private readonly TypeBuilder _tb;

		private readonly Dictionary<string, FieldBuilder>		_fieldBuilderMap	= new Dictionary<string, FieldBuilder>();
		private readonly Dictionary<StubKind, IStubBodyWriter>	_stubsWritersMap	= new Dictionary<StubKind, IStubBodyWriter>();
		private AssemblyBuilder _ab;

		public ILProxyGenerator(Type targetObjectType)
		{
			_tb = CreateTypeBuilder(targetObjectType);
			_stubsWritersMap[StubKind.Proxy] = new StubProxyBodyWriter(_fieldBuilderMap);
			_stubsWritersMap[StubKind.Silent] = new StubSilentBodyWriter();
			_stubsWritersMap[StubKind.Exception] = new StubExceptionBodyWriter();
		}

		public void GenerateConstructor()
		{
			WriteField<Object>(FieldNames.InstanceName);
			WriteField<Type>(FieldNames.InstanceTypeName);
            WriteField<Dictionary<string, MethodInfo>>(FieldNames.MethodsDictionaryName);
			WriteConstructor();
		}

		public void ImplementInterface(Type interfaceType, MetadataParserResult result)
		{
			_tb.AddInterfaceImplementation(interfaceType);

			Dictionary<string, MethodBuilder> getSetInfos = new Dictionary<string, MethodBuilder>();
			foreach (KeyValuePair<MethodInfo, StubKind> pair in result.InterfaceMethodInfos)
			{
				if (ReflectionSupport.IsPropertyMethod(pair.Key))
				{
                    MethodBuilder method = WriteMethod(pair.Key, PropAttribute, pair.Value);
					getSetInfos[ReflectionSupport.GetSignature(pair.Key)] = method;
				}
                else WriteMethod(pair.Key, MethAttribute, pair.Value);
			}

			foreach (PropertyInfo pi in result.PropertyInfos)
			{
				Type[] parTypes = GetParametrsTypes(pi.GetIndexParameters());
				PropertyBuilder propBuilder = _tb.DefineProperty(pi.Name, pi.Attributes,
					pi.PropertyType, parTypes);

				if (pi.CanRead)
				{
					string getSignature = ReflectionSupport.GetGetSignature(pi);
					MethodBuilder methodInfo = getSetInfos[getSignature];
					propBuilder.SetGetMethod(methodInfo);
				}

				if (pi.CanWrite)
				{
					string setSignature = ReflectionSupport.GetSetSignature(pi);
					MethodBuilder methodInfo = getSetInfos[setSignature];
					propBuilder.SetSetMethod(methodInfo);
				}
			}
		}

		public Type CreateType()
		{ 
			return _tb.CreateType(); 
		}



		private TypeBuilder CreateTypeBuilder(Type oType)
		{
			AssemblyName aName = new AssemblyName("DynamicProxyAssembly");

			_ab = AppDomain.CurrentDomain.DefineDynamicAssembly(aName, AssemblyBuilderAccess.RunAndSave);
			ModuleBuilder mb = _ab.DefineDynamicModule(aName.Name, aName.Name + ".dll");
			TypeBuilder tb = mb.DefineType("Proxy" + oType.Name + "Type", TypeAttributes.Public);
			
			return tb;
		}

		private MethodBuilder WriteMethod(MethodInfo mi, MethodAttributes ma, StubKind stubKind)
		{
			MethodBuilder methodBuilder = WriteMethodSignature(mi, ma, mi.GetParameters());
			ILGenerator ilGen = methodBuilder.GetILGenerator();
            _stubsWritersMap[stubKind].Write(ilGen, mi);
			return methodBuilder; 
		}

		private MethodBuilder WriteMethodSignature(MethodInfo mi, MethodAttributes ma, ParameterInfo[] pars)
		{
			Type[] parTypes = GetParametrsTypes(pars);
			Type returnType = mi.ReturnType != typeof(void) ? mi.ReturnType : null;
			MethodBuilder methBuilder = _tb.DefineMethod(mi.Name, ma, returnType, parTypes);

			if (mi.IsGenericMethod)
			{
				Type[] genericArguments = mi.GetGenericArguments();

				string[] typeParamNames = new string[genericArguments.Length];
				for (int i = 0; i < genericArguments.Length; i++)
					typeParamNames[i] = genericArguments[i].Name;

				methBuilder.DefineGenericParameters(typeParamNames);
			}

			for (int i = 0; i < pars.Length; i++)
				methBuilder.DefineParameter(i + 1, pars[i].Attributes, pars[i].Name);

			return methBuilder;
		}

		private static Type[] GetParametrsTypes(ParameterInfo[] pars)
		{
			if (pars == null) return null;

			Type[] parTypes = new Type[pars.Length];
			for (int i = 0; i < pars.Length; i++)
				parTypes[i] = pars[i].ParameterType;
			return parTypes;
		}

		private void WriteField<T>(string name)
		{
			FieldBuilder fb = _tb.DefineField(name, typeof(T), FieldAttributes.Private);
			_fieldBuilderMap[name] = fb;
		}

		private void WriteConstructor()
		{
			ConstructorBuilder conBuilder = _tb.DefineConstructor(
                MethodAttributes.Public, CallingConventions.Standard,
				new[] { typeof(Object), typeof(Dictionary<string, MethodInfo>) });

			ILGenerator cgen = conBuilder.GetILGenerator();
			cgen.Emit(OpCodes.Ldarg_0);
			cgen.Emit(OpCodes.Ldarg_1);
			cgen.Emit(OpCodes.Stfld, _fieldBuilderMap[FieldNames.InstanceName]);
            cgen.Emit(OpCodes.Ldarg_0);
            cgen.Emit(OpCodes.Ldarg_2);
            cgen.Emit(OpCodes.Stfld, _fieldBuilderMap[FieldNames.MethodsDictionaryName]);
			cgen.Emit(OpCodes.Ldarg_0);
            cgen.Emit(OpCodes.Ldarg_0);
			cgen.Emit(OpCodes.Ldfld, _fieldBuilderMap[FieldNames.InstanceName]);
			cgen.Emit(OpCodes.Callvirt, typeof(MethodInfo).GetMethod("GetType"));
			cgen.Emit(OpCodes.Stfld, _fieldBuilderMap[FieldNames.InstanceTypeName]);
			cgen.Emit(OpCodes.Ret);
		}
	}
}
