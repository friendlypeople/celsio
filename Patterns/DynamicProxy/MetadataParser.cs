// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Reflection;
using Celsio.Commons.Utils;

namespace Celsio.Patterns.DynamicProxy
{
	internal sealed class MetadataParserResult
	{
		public List<PropertyInfo> PropertyInfos = new List<PropertyInfo>();
		public Dictionary<MethodInfo, StubKind> InterfaceMethodInfos = new Dictionary<MethodInfo, StubKind>();
        public Dictionary<string, MethodInfo> InstanceMethodInfos = new Dictionary<string, MethodInfo>();
	}

	internal sealed class MetadataParser
	{
		private readonly Type _iType;
		private readonly Type _oType;

		public MetadataParser(Type iType, Type oType)
		{
			_iType = iType;
			_oType = oType;
		}

		public MetadataParserResult Run()
		{
			Dictionary<string, MethodInfo> oMethodsMap = new Dictionary<string, MethodInfo>();
			Dictionary<string, PropertyInfo> oPropsMap = new Dictionary<string, PropertyInfo>();

			foreach (MethodInfo methodInfo in _oType.GetMethods())
			{
				string signature = ReflectionSupport.GetSignature(methodInfo);
				oMethodsMap[signature] = methodInfo;
			}

			foreach (PropertyInfo propInfo in _oType.GetProperties())
			{
				string signature = propInfo.Name;
				oPropsMap[signature] = propInfo;
			}

			MetadataParserResult parserResult = new MetadataParserResult();
			foreach (MethodInfo methodInfo in _iType.GetMethods())
			{
				string signature = ReflectionSupport.GetSignature(methodInfo);
				parserResult.InterfaceMethodInfos[methodInfo] = oMethodsMap.ContainsKey(signature)
				                                                	? StubKind.Proxy
				                                                	: StubKind.Silent;
			}
			parserResult.PropertyInfos = new List<PropertyInfo>(_iType.GetProperties());
			parserResult.InstanceMethodInfos = oMethodsMap;

			return parserResult;
		}
	}
}
