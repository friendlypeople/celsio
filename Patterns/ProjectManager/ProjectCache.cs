// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;

namespace Celsio.Patterns.ProjectManager
{
    /// <summary>
    /// Класс-кэш открытых проектов
    /// </summary>
    /// <typeparam name="TProject">Класс проекта</typeparam>
    public class ProjectCache<TProject> where TProject : class, IProject
    {
        /// <summary>
        /// Таблица проектов
        /// </summary>
        private readonly SortedDictionary<string, TProject> _projectTable = new SortedDictionary<string, TProject>();
        /// <summary>
        /// Текущий проект
        /// </summary>
        private TProject _currentProject;
        /// <summary>
        /// Проект по умолчанию
        /// </summary>
        private TProject _defaultProject;


        /// <summary>
        /// Текущий проект
        /// </summary>
        public TProject CurrentProject
        {
			get { return _currentProject ?? DefaultProject; }
            set { _currentProject = value; }
        }

        /// <summary>
        /// Проект по умолчанию
        /// </summary>
        public TProject DefaultProject
        {
            get { return _defaultProject; }
            set { _defaultProject = value; }
        }

        /// <summary>
        /// Проверяет, инициализирован ли проект с заданым именем
        /// </summary>
        /// <param name="name">Имя проекта</param>
        /// <returns>
        /// 	<c>true</c> если операции выполнена успешна, иначе <c>false</c>
        /// </returns>
        public bool ContainsProject(string name)
        {
            return _projectTable.ContainsKey(name);
        }

        /// <summary>
        /// Добавить проект в список проектов
        /// </summary>
        /// <param name="project">Новый проект</param>
        /// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c></returns>
        public void AddProject(TProject project)
        {
            if (!_projectTable.ContainsKey(project.Name))
            {
                _projectTable.Add(project.Name, project);
                _currentProject = project;
            }
        }

        /// <summary>
        /// Удалить проект из списка
        /// </summary>
        /// <param name="name">Имя проекта</param>
        /// <returns><c>true</c> если операции выполнена успешна, иначе <c>false</c> </returns>
        public bool RemoveProject(string name)
        {
            if (string.IsNullOrEmpty(name)) return false;
            if (_projectTable.ContainsKey(name))
            {
                if (_currentProject == _projectTable[name])
                    _currentProject = null;
                _projectTable.Remove(name);
                
                return true;
            }
            return false;
        }

        public TProject this[string name]
        {
            get 
            {
                if (string.IsNullOrEmpty(name))
                    throw new ArgumentNullException("name");
                return _projectTable[name];
            }
        }
    }
}
