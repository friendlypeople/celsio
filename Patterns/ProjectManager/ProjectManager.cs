// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


namespace Celsio.Patterns.ProjectManager
{
    public delegate void DataBaseEventHandler<TProject>(TProject project);

    /// <summary>
    /// Класс менеджер проектов
    /// </summary>
    /// <typeparam name="TProject">Класс проекта</typeparam>
    public class ProjectManager<TProject> : IProjectManager<TProject> where TProject : class, IProject
    {
        /// <summary>
        /// Кэш проектов
        /// </summary>
        private readonly ProjectCache<TProject> _projectCache = new ProjectCache<TProject>();
        /// <summary>
        /// Строитель проектов
        /// </summary>
        private readonly IProjectBuilder<TProject> _projectBuilder;
        /// <summary>
        /// Поддерживается однодокументный режим
        /// </summary>
        private readonly bool _isSdi;

        #region Properties

        /// <summary>
        /// Кэш проектов
        /// </summary>
        protected ProjectCache<TProject> ProjectCache
        {
            get { return _projectCache; }
        }

        /// <summary>
        /// Строитель проектов
        /// </summary>
        public IProjectBuilder<TProject> ProjectBuilder
        {
            get { return _projectBuilder; }
        }

        /// <summary>
        /// Текущий проект
        /// </summary>
        public TProject CurrentProject
        {
            get { return _projectCache.CurrentProject; }
        }

        /// <summary>
        /// Проект по умолчанию
        /// </summary>
        public TProject DefaultProject
        {
            get { return _projectCache.DefaultProject; }
        }

        #endregion

        public ProjectManager(IProjectBuilder<TProject> projectBuilder, bool isSdi)
            : this(projectBuilder)
        {
            _isSdi = isSdi;
        }

        public ProjectManager(IProjectBuilder<TProject> projectBuilder)
        {
            _projectBuilder = projectBuilder;
            _projectCache.DefaultProject = ProjectBuilder.BuildDefault();
        }

        /// <summary>
        /// Создать проект
        /// </summary>
        /// <param name="name">Имя проекта</param>
        public virtual void CreateProject(string name)
        {
            TProject project = ProjectBuilder.Build(name);
            OpenProject(project);
        }

        /// <summary>
        /// Создать проект
        /// </summary>
        /// <param name="name">Имя проекта</param>
        /// <param name="options">Имя проекта</param>
        public virtual void CreateProject(string name, object options)
        {
            TProject project = ProjectBuilder.Build(name, options);
            OpenProject(project);
        }

        /// <summary>
        /// Открыть проект
        /// </summary>
        /// <param name="project"></param>
        public virtual void OpenProject(TProject project)
        {
            if (_isSdi)
                CloseCurrentProject();
            _projectCache.AddProject(project);
            OpenProjectHandler(project);
        }

        /// <summary>
        /// Активировать проект
        /// </summary>
        /// <param name="name"></param>
        public virtual bool ActivateProject(string name)
        {
            if (!_isSdi && _projectCache.ContainsProject(name))
            {
                _projectCache.CurrentProject = _projectCache[name];
                ActivateProjectHandler(CurrentProject);
                return true;
            }
            _projectCache.CurrentProject = null;
            return false;
        }

        /// <summary>
        /// Закрыть проект
        /// </summary>
        /// <param name="name"></param>
        public virtual bool CloseProject(string name)
        {
            if (_projectCache.ContainsProject(name))
            {
                TProject project = _projectCache[name];
                CloseProjectHandler(project);
                project.Close();
            }
            return _projectCache.RemoveProject(name);
        }

        /// <summary>
        /// Закрыть текущий проект
        /// </summary>
        public bool CloseCurrentProject()
        {
            if(CurrentProject != null)
                return CloseProject(CurrentProject.Name);
            return false;
        }

        /// <summary>
        /// Переименовать проект
        /// </summary>
        /// <param name="oldName">Старое имя проекта</param>
        /// <param name="newName">Новое имя проекта</param>
        public bool RenameProject(string oldName, string newName)
        {
            if (_projectCache.ContainsProject(oldName))
            {
                TProject current = _projectCache.CurrentProject;

                TProject project = _projectCache[oldName];
                _projectCache.RemoveProject(oldName);
                project.Name = newName;
                _projectCache.AddProject(project);

                _projectCache.CurrentProject = current;
                return true;
            }
            return false;
        }

        public virtual void Initialize() { }

        public virtual void Terminate() { }

        /// <summary>
        /// Активация события "документ становится активный"
        /// </summary>
        /// <param name="project"></param>
        protected void ActivateProjectHandler(TProject project)
        {
            if (OnActivateProject != null)
                OnActivateProject(project);
        }

        /// <summary>
        /// Активация события "открытие документа"
        /// </summary>
        /// <param name="project"></param>
        protected void OpenProjectHandler(TProject project)
        {
            if (OnOpenProject != null)
                OnOpenProject(project);
        }

        /// <summary>
        /// Активация события "закрытие документа"
        /// </summary>
        /// <param name="project"></param>
        protected void CloseProjectHandler(TProject project)
        {
            if (OnCloseProject != null)
                OnCloseProject(project);
        }

        public event DataBaseEventHandler<TProject> OnActivateProject;
        public event DataBaseEventHandler<TProject> OnOpenProject;
        public event DataBaseEventHandler<TProject> OnCloseProject;
    }
}
