// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Xml;
using Celsio.Patterns.VersionController;
using Celsio.Serializers;

namespace Celsio.Patterns.VersionedExporterDispatcher
{
	public class VersionedExporterDispatcher<TObject, TBaseType>
		 : IVersionedExporterDispatcher<TObject>, IXmlSerializer<TBaseType>
		where TBaseType : class
		where TObject : class
	{
		private const string VersionAttributeName = "Version";

		protected static readonly VersionController<IVersionedXmlSirializer<TObject>> VersionController =
			new VersionController<IVersionedXmlSirializer<TObject>>();

		private string GetExporterVersion(XmlNode node)
		{
			XmlNode sn = node.SelectSingleNode(VersionAttributeName);
			return sn == null ? VersionController.MinorVersion : sn.InnerText;
		}

		private XmlNode GetSegmentNode(XmlNode node)
		{
			string str = typeof(TObject).Name;
			XmlNode sn = node.SelectSingleNode(str);
			return sn ?? node;
		}

		public void Load(XmlNode node, TObject data)
		{
			XmlNode xmlNode = GetSegmentNode(node);
			string formatterVersion = GetExporterVersion(xmlNode);
			if (VersionController.IsVersion(formatterVersion))
			{
				IVersionedXmlSirializer<TObject> formatter = VersionController.GetVersion(formatterVersion);
				formatter.Load(xmlNode, data);
			}
		}

		public void Save(XmlWriter writer, TObject data)
		{
			string lastVersion = VersionController.MajorVersion;
			string str = typeof(TObject).Name;
			if (VersionController.IsVersion(lastVersion))
			{
				writer.WriteStartElement(str);
				writer.WriteStartElement(VersionAttributeName);
				writer.WriteValue(lastVersion);
				writer.WriteEndElement();
				IVersionedXmlSirializer<TObject> formatter = VersionController.GetVersion(lastVersion);
				formatter.Save(writer, data);
				writer.WriteEndElement();
			}
		}

		protected TObject SafelyCast(TBaseType obj)
		{
			if (obj == null) throw new ArgumentNullException("obj");
			TObject page = obj as TObject;
			if (page != null) return page;

			throw new Exception("ExporterDispatcher type cast error");
		}

		void IXmlSerializer<TBaseType>.Save(XmlWriter writer, TBaseType obj)
		{
			Save(writer, SafelyCast(obj));
		}

		void IXmlSerializer<TBaseType>.Load(XmlNode node, TBaseType obj)
		{
			Load(node, SafelyCast(obj));
		}

		
	}
}
