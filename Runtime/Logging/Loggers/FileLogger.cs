// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.IO;
using Celsio.Runtime.Diagnostics;
using Celsio.Runtime.Logging.MessageFormatters;

namespace Celsio.Runtime.Logging.Loggers
{
    public class FileLogger : LoggerBase
    {
        protected TextWriter _streamWriter;
        private readonly object _lock = new object();
        private readonly string _filename = string.Empty;

        public string Filename
        {
            get { return _filename; }
        }

        protected override void WriteMessageImpl(Severity severity, string message, ExceptionInfo exception)
        {
            lock (_lock)
            {
                // ������������ ������ ������ ������ ������
                _streamWriter.WriteLine(message);
                if (exception != null)
                {
					_streamWriter.WriteLine(exception.ToString());
                }
                _streamWriter.Flush();
            }
        }

        protected FileLogger(IMessageFormatter formatter, TextWriter streamWriter, string filename)
            : base(formatter)
        {
            if (streamWriter == null) throw new ArgumentNullException("streamWriter");
            if (filename == null) throw new ArgumentNullException("filename");
            _streamWriter = streamWriter;
            _filename = filename;
        }

        public static FileLogger Create(string fileName)
        {
            return Create(fileName, new DefaultMessageFormatter());
        }

        public static FileLogger Create(string fileName, IMessageFormatter formatter)
        {
            if (fileName == null) throw new ArgumentNullException("fileName");

            int processCount = 0;
            string copyNumber = "[" + processCount + "]";
            string fullPath = Path.GetDirectoryName(fileName);
            string tempName = Path.GetFileNameWithoutExtension(fileName);
            string tempExt = Path.GetExtension(fileName);

            while (true)
            {
                try
                {
                    fileName = Path.Combine(fullPath, tempName + copyNumber + tempExt);
                    TextWriter sw = new StreamWriter(fileName, true);
                    return new FileLogger(formatter, sw, fileName);
                }
                catch (IOException)
                {
                    copyNumber = "[" + processCount + "]";
                    processCount++;
                    if (processCount > 10) break;
                }
            }
            throw new Exception("�� ������� ������� ���� ����");
            
        }
        public override void TearDownLogger()
        {
            base.TearDownLogger();
            if (_streamWriter != null)
            {
                _streamWriter.Close();
                _streamWriter = null;

            }

        }

    }
}
