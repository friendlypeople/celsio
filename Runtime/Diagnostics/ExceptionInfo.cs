// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Text;

namespace Celsio.Runtime.Diagnostics
{
    public class ExceptionInfo
    {
        private readonly string             _excType;
        private readonly string             _excMessage;
        private readonly StackTraceInfo     _excStackTrace;
        private readonly ExceptionInfo      _innerException;


        /// <summary>
        /// ������������� ���������� ������ <see cref="ExceptionInfo"/>.
        /// </summary>
        /// <param name="exception">����������</param>
        /// <exception cref="ArgumentNullException">���������� ���� <paramref name="exception"/> ����� ����</exception>
        public ExceptionInfo(Exception exception)
        {
            if (exception == null) throw new ArgumentNullException("exception");

            _excType = exception.GetType().FullName;
            _excMessage = ExceptionRoutine.SafelyExtractMessage(exception);
            _excStackTrace = new StackTraceInfo(ExceptionRoutine.SafelyExtractStackTrace(exception));
            //properties = ExtractExceptionProperties(exception).AsReadOnly();

            if (exception.InnerException != null)
                _innerException = new ExceptionInfo(exception.InnerException);
        }


        /// <summary>
        /// ��������� ���� ����������
        /// </summary>
        /// <value>��� ����������</value>
        public string Type 
        {
            get { return _excType; }
        }

        /// <summary>
        /// ��������� ������ ��������� ����������
        /// </summary>
        /// <value>����� ��������� ����������</value>
        public string Message
        {
            get { return _excMessage; }
        }

        /// <summary>
        /// ��������� ����� ������ ����������
        /// </summary>
        /// <value>���� ������ ����������</value>
        public StackTraceInfo StackTrace
        {
            get { return _excStackTrace; }
        }

        /// <summary>
        /// ��������� ��������� ����������, ��� null ���� ����� �����������
        /// </summary>
        /// <value>�������� ����������</value>
        public ExceptionInfo InnerException
        {
            get { return _innerException; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(4);
            sb.AppendFormat("{0}: {1}\r\n", _excType, _excMessage);
            sb.Append(_excStackTrace + "\r\n");
            if (_innerException != null)
            {
                sb.Append(" --- InnerException ---\r\n");
                sb.Append(_innerException.ToString());
            }
            return sb.ToString();
        }



    }
}
