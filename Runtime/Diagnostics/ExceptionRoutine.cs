// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;

namespace Celsio.Runtime.Diagnostics
{
    static class ExceptionRoutine
    {
        /// <summary>
        /// ���������� ��������������� ���������� � ������
        /// </summary>        
        /// <param name="exception">����������</param>
        /// <returns>The string contents.</returns>
        /// <exception cref="ArgumentNullException">���������� ���� <paramref name="exception"/> ����� ����</exception>
        public static string SafeToString(Exception exception)
        {
            if (exception == null) throw new ArgumentNullException("exception");
            try
            {
                return exception.ToString();
            }
            catch (Exception ex2)
            {
                return String.Format("�������� ���������� '{0}'. ������ ���������� ���������� �.�. �������� ���������� '{1}' ��� ������� ��������� ����������",
                    exception.GetType().FullName, ex2.GetType().FullName);
            }
        }

        /// <summary>
        /// ��������� <see cref="Exception.Message"/> ���������� ����������.
        /// </summary>
        /// <param name="exception">����������</param>
        /// <returns>��������� ����������</returns>
        /// <exception cref="ArgumentNullException">���������� ���� <paramref name="exception"/> ����� ����</exception>
        public static string SafelyExtractMessage(Exception exception)
        {
            if (exception == null) throw new ArgumentNullException("exception");
            try
            {
                return exception.Message;
            }
            catch (Exception ex2)
            {
                return String.Format("�������� ���������� '{0}'. ������ ���������� ���������� �.�. �������� ���������� '{1}' ��� ������� ��������� ����������",
                    exception.GetType().FullName, ex2.GetType().FullName);
            }
        }

        /// <summary>
        /// ��������� <see cref="Exception.StackTrace"/> ���������� ����������.
        /// </summary>
        /// <param name="exception">����������</param>
        /// <returns>���� �������</returns>
        /// <exception cref="ArgumentNullException">���������� ���� <paramref name="exception"/> ����� ����</exception>
        public static string SafelyExtractStackTrace(Exception exception)
        {
            if (exception == null) throw new ArgumentNullException("exception");
            try
            {
                if (exception.StackTrace != null)
                    return exception.StackTrace;
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }


    }
}
