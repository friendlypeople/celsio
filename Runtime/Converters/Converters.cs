// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Drawing;

namespace Celsio.Runtime.Converters
{
	//delegate TypeConverter CreateConverter();

	public class TypeConverterProvider
	{
		private static readonly Dictionary<Type, TypeConverter> SpecificConverterTable = new Dictionary<Type, TypeConverter>(10);

		#region �������� ������

		private static T ConvertFrom<T>(object value, TypeConverter converter)
		{
			try
			{
				if (converter == null)
				{
					System.Diagnostics.Debug.Assert(false, "ConvertFrom", "�� ������� �������� ��������� ��� ���� " + typeof(T));
					return default(T);
				}

				object obj = converter.ConvertFrom(value);

				if (obj == null)
				{
					System.Diagnostics.Debug.Assert(false, "ConvertFrom", "�� ������� ������������� ������ ���� " + typeof(T));
					return default(T);
				}
				return (T)obj;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.Assert(false, "�� ������� ������������� ������ ���� " + typeof(T), ex.Message);
				return default(T);
			}
		}

		private static TR ConvertTo<TR, T>(T value, TypeConverter converter)
		{
			try
			{
				if (converter == null)
				{
					System.Diagnostics.Debug.Assert(false, "ConvertTo", "�� ������� �������� ��������� ��� ���� " + typeof(T).ToString());
					return default(TR);
				}
				object obj = converter.ConvertTo(value, typeof(TR));
				if (obj == null)
				{
					System.Diagnostics.Debug.Assert(false, "ConvertTo", "�� ������� ������������� ������ ���� " + typeof(T));
					return default(TR);
				}
				return (TR)obj;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.Assert(false, "�� ������� ������������� ������ ���� " + typeof(T), ex.Message);
				return default(TR);
			}
		}

		#endregion

		#region ������� ������� � �����������
		/// <summary>
		/// ������������ ����������� ��������� �����
		/// </summary>
		/// <returns>��������� ��������� ����</returns>
		public static TypeConverter GetStandartConverter(Type type)
		{
			return TypeDescriptor.GetConverter(type);
		}

		/// <summary>
		/// ������������ ����������� ��������� ������������
		/// </summary>
		/// <param name="type">���</param>
		/// <returns>��������� ��������� ����</returns>
		public static TypeConverter GetStandartEnumConverter(Type type)
		{
			return new EnumConverter(type);
		}

		/// <summary>
		/// ������������ ������������� ��������� ������������
		/// ������������ ������� Description
		/// </summary>
		/// <param name="type">���</param>
		/// <returns>��������� ��������� ����</returns>
		public static TypeConverter GetSpecificEnumConverter(Type type)
		{
			return new EnumTypeConverter(type);
		}

		/// <summary>
		/// ������������ ������������� ����������� ��� �����
		/// </summary>
		/// <param name="type">���</param>
		/// <returns>��������� ��������� ����</returns>
		public static TypeConverter GetSpecificConverter(Type type)
		{
			TypeConverter converter;
			if (SpecificConverterTable.TryGetValue(type, out converter))
			{
				// ���������� ���
				return converter;
			}
			// ���������� �����������
			return TypeDescriptor.GetConverter(type);
		}

		/// <summary>
		/// ������������ ������������� ����������� ��� �����
		/// </summary>
		/// <typeparam name="T">���</typeparam>
		/// <returns>��������� ��������� ����</returns>
		public static TypeConverter GetSpecificConverter<T>()
		{
			return GetSpecificConverter(typeof(T));
		}

		#endregion

		#region ������� ������� �����������

		/// <summary>
		/// �������������� � �������������� ����������� �����������
		/// </summary>
		/// <typeparam name="T">��� �������</typeparam>
		/// <param name="value">������������� ������</param>
		/// <returns>��������������� ��������</returns>
		public static T StandartConvertFrom<T>(object value)
		{
			return (T)GetStandartConverter(typeof(T)).ConvertFrom(value);
		}


		/// <summary>
		/// �������������� � �������������� ����������� �����������
		/// </summary>
		/// <param name="type">��� �������</param>
		/// <param name="value">������������� ������</param>
		/// <returns>��������������� ��������</returns>
		public static object StandartConvertFrom(Type type, object value)
		{
			return GetStandartConverter(type).ConvertFrom(value);
		}


		/// <summary>
		/// �������������� � �������������� ������������� �����������
		/// </summary>
		/// <typeparam name="T">���������� ���</typeparam>
		/// <param name="value">������������� ������</param>
		/// <returns>��������������� ��������</returns>
		public static T SpecificConvertFrom<T>(object value)
		{
			//return ConvertFrom<TYPE>(value, GetSpecificConverter(typeof(TYPE)));
			return (T)GetSpecificConverter(typeof(T)).ConvertFrom(value);
		}

		/// <summary>
		/// �������������� � �������������� ������������� �����������
		/// </summary>
		/// <param name="type">��� �������</param>
		/// <param name="value">������������� ������</param>
		/// <returns>��������������� ��������</returns>
		public static object SpecificConvertFrom(Type type, object value)
		{
			return GetSpecificConverter(type).ConvertFrom(value);
		}


		/// <summary>
		/// ��������������� ���� �� ������ � �������������� �������������� ����������
		/// </summary>
		/// <typeparam name="T">���������� ���.</typeparam>
		/// <param name="value">������������� ������</param>
		/// <returns>��������������� ��������</returns>
		public static T SpecificConvertFromString<T>(string value)
		{
			return ConvertFrom<T>(value, GetSpecificConverter(typeof(T)));
		}

		/// <summary>
		/// ��������������� ���� �� ������ � �������������� �������������� ����������
		/// </summary>
		/// <param name="type">���������� ���.</param>
		/// <param name="value">������������� ������</param>
		/// <returns>��������������� ��������</returns>
		public static object SpecificConvertFromString(Type type, string value)
		{
			return GetSpecificConverter(type).ConvertFromString(value);
		}

		/// <summary>
		/// ��������������� ���� � ������ � �������������� �������������� ����������
		/// </summary>
		/// <typeparam name="T">��� �������</typeparam>
		/// <param name="value">������������� ��������</param>
		/// <returns>��������������� ������</returns>
		public static string SpecificConvertToString<T>(T value)
		{
			return ConvertTo<string, T>(value, GetSpecificConverter(typeof(T)));
		}
		/// <summary>
		/// ��������������� ���� � ������ � �������������� �������������� ����������
		/// </summary>
		/// <param name="type">���������� ���.</param>
		/// <param name="value">������������� ��������</param>
		/// <returns>��������������� ������</returns>
		public static string SpecificConvertToString(Type type, object value)
		{
			return GetSpecificConverter(type).ConvertToString(value);
		}


		/// <summary>
		/// ��������������� ������������ �� ������ � �������������� �������������� ����������
		/// </summary>
		/// <typeparam name="T">���������� ���.</typeparam>
		/// <param name="value">������������� ������</param>
		/// <returns>��������������� ��������</returns>
		public static T SpecificEnumConvertFromString<T>(string value)
		{
			return ConvertFrom<T>(value, GetSpecificEnumConverter(typeof(T)));
		}

		/// <summary>
		/// ��������������� ������������ � ������ � �������������� �������������� ����������
		/// </summary>
		/// <typeparam name="T">���������� ���.</typeparam>
		/// <param name="value">������������� ��������</param>
		/// <returns>��������������� ������</returns>
		public static string SpecificEnumConvertToString<T>(T value)
		{
			return ConvertTo<string, T>(value, GetSpecificEnumConverter(typeof(T)));
		}



		#endregion

		/// <summary>
		/// ������������� <see cref="TypeConverterProvider&lt;TYPE&gt;"/> ������.
		/// </summary>
		static TypeConverterProvider()
		{
			try
			{
				SpecificConverterTable.Add(typeof(bool),	new BooleanTypeConverter());
				SpecificConverterTable.Add(typeof(double),	new DoubleTypeConverter());
				SpecificConverterTable.Add(typeof(Type),	new SystemTypeConverter());
				SpecificConverterTable.Add(typeof(Color),	new ColorTypeConverter());

			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.Assert(false, "������ ���������� ���� ����������", ex.Message);
			}
		}

		public static bool AddConverter<T, TConverter>() where TConverter : TypeConverter, new()
		{
			try
			{
				SpecificConverterTable.Add(typeof(T), new TConverter());
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.Assert(false, "������ ���������� ���� ����������", ex.Message);
				return false;
			}
			return true;
		}

		public static bool AddConverter<T>(TypeConverter converter)
		{
			try
			{
				SpecificConverterTable.Add(typeof(T), converter);
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.Assert(false, "������ ���������� ���� ����������", ex.Message);
				return false;
			}
			return true;
		}
	}
}
