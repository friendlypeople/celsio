// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 




//using System;
//using System.ComponentModel;
//using System.Globalization;
//using System.Text;

//namespace Converters
//{
//    #region ��������� HandleTypeConverter
//    /// <summary>
//    /// ��������������� �������� Handle
//    /// </summary>
//    public class HandleTypeConverter : TypeConverter
//    {
//        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
//        {
//            if (sourceType == typeof(string))
//                return true;
//            return false;
//        }

//        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
//        {
//            if (value is Autodesk.AutoCAD.DatabaseServices.Handle)
//                return (Autodesk.AutoCAD.DatabaseServices.Handle)value;

//            if (!(value is String))
//                return base.ConvertFrom(context, culture, value);

//            // �������� ������ � Hex
//            string hString = (string)value;
//            long lHandle = 0;
//            try
//            {
//                lHandle = Convert.ToInt64(hString, 16); // �� String Hex � ���������� �����
//                Autodesk.AutoCAD.DatabaseServices.Handle handle = new Autodesk.AutoCAD.DatabaseServices.Handle(lHandle);
//                return handle;
//            }
//            catch (Exception ex)
//            {
//                NeoAcadUtils.EditorRoutine.WriteMessage("���������� ������������� String � Handle: " + ex.Message);
//                System.Diagnostics.Debug.WriteLine("���������� ������������� String � Handle: " + ex.Message);
//                return lHandle;
//            }
//        }

//        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
//        {
//            if (destinationType == typeof(string))
//                return true;
//            return false;
//        }

//        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
//        {
//            if (value is Autodesk.AutoCAD.DatabaseServices.Handle)
//            {
//                Autodesk.AutoCAD.DatabaseServices.Handle handle = (Autodesk.AutoCAD.DatabaseServices.Handle)value;
//                // ������ Handle � Hex ����
//                if (handle != null)
//                    return handle.ToString();
//            }
//            return String.Empty;
//        }
//    }
//    #endregion

//    #region ��������� Matrix3dTypeConverter
//    /// <summary>
//    /// ��������������� �������� Matrix3d
//    /// </summary>
//    public class Matrix3dTypeConverter : TypeConverter
//    {
//        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
//        {
//            if (sourceType == typeof(string))
//                return true;
//            return false;
//        }

//        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
//        {
//            if (!(value is String))
//                return base.ConvertFrom(context, culture, value);

//            // ������ �������������� ������ ������������ �����
//            string doubleArrayString = (string)value;

//            // ������ ���� ��������� ;
//            string[] doubleArrayAsStringElements = doubleArrayString.Split(';');

//            // ��������� ������ ������������ �����
//            double[] doubleArray = new double[doubleArrayAsStringElements.Length];

//            for (int i = 0; i < doubleArrayAsStringElements.Length; i++)
//            {
//                string dblStr = doubleArrayAsStringElements[i];
//                // ����������� ������������� ������������ ����� � ������ � ����������� � �������� ����������� (������������� �����������)
//                dblStr = NeoAcadUtils.Controller.NormDouble(dblStr);

//                double test = 0;
//                if (double.TryParse(dblStr, out test))
//                {
//                    doubleArray[i] = test;
//                }
//            }

//            Autodesk.AutoCAD.Geometry.Matrix3d matrix = new Autodesk.AutoCAD.Geometry.Matrix3d(doubleArray);
//            return matrix;
//        }

//        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
//        {
//            if (destinationType == typeof(string))
//                return true;
//            return false;
//        }

//        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
//        {
//            if (!(value is Autodesk.AutoCAD.Geometry.Matrix3d)) return string.Empty;

//            Autodesk.AutoCAD.Geometry.Matrix3d matrix = (Autodesk.AutoCAD.Geometry.Matrix3d)value;

//            // ������� � ���� �������
//            double[] matrixArray = matrix.ToArray();

//            // ������������� ������� � ���� ������ ����������� ';'
//            string doubleArrayString = string.Empty;

//            foreach (double el in matrixArray)
//            {
//                string elStr = el.ToString();
//                doubleArrayString += elStr + ";";
//            }
//            // ������� ��������� ;
//            doubleArrayString = doubleArrayString.TrimEnd(';');
//            return doubleArrayString;
//        }
//    }
//    #endregion

//    #region ��������� Point3dTypeConverter
//    /// <summary>
//    /// ��������������� �������� Point3d
//    /// </summary>
//    public class Point3dTypeConverter : TypeConverter
//    {
//        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
//        {
//            if (sourceType == typeof(string))
//                return true;
//            return false;
//        }


//        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
//        {
//            if (!(value is String)) return base.ConvertFrom(context, culture, value);

//            string ptString = (string)value;

//            // ����������� ������������� ������������ ����� � ������ � ����������� � �������� ����������� (������������� �����������)
//            ptString = NeoAcadUtils.Controller.NormDouble(ptString);

//            // ������ ���� ��������� ;
//            string[] ptSost = ptString.Split(';');

//            double x = 0;
//            double y = 0;
//            double z = 0;
//            double test = 0;
//            if (double.TryParse(ptSost[0], out test))
//                x = test;

//            if (double.TryParse(ptSost[1], out test))
//                y = test;
//            if (double.TryParse(ptSost[2], out test))
//                z = test;

//            Autodesk.AutoCAD.Geometry.Point3d pt = new Autodesk.AutoCAD.Geometry.Point3d(x, y, z);
//            return pt;
//        }



//        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
//        {
//            if (destinationType == typeof(string))
//                return true;
//            return false;
//        }

//        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
//        {
//            string ptString = String.Empty;
//            if (value is Autodesk.AutoCAD.Geometry.Point3d)
//            {
//                Autodesk.AutoCAD.Geometry.Point3d pt = (Autodesk.AutoCAD.Geometry.Point3d)value;
//                string xStr = pt.X.ToString();
//                string yStr = pt.Y.ToString();
//                string zStr = pt.Z.ToString();
//                ptString = xStr + ";" + yStr + ";" + zStr;
//            }
//            return ptString;
//        }
//    }
//    #endregion

//    #region ��������� Vector3dTypeConverter
//    /// <summary>
//    /// ��������������� �������� Vector3d
//    /// </summary>
//    public class Vector3dTypeConverter : TypeConverter
//    {
//        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
//        {
//            if (sourceType == typeof(string))
//                return true;
//            return false;
//        }

//        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
//        {
//            if (value is String)
//            {
//                string vecString = (string)value;
//                // ����������� ������������� ������������ ����� � ������ � ����������� � �������� ����������� (������������� �����������)
//                vecString = NeoAcadUtils.Controller.NormDouble(vecString);
//                // ������ ���� ��������� ;
//                string[] ptSost = vecString.Split(';');

//                double x = 0;
//                double y = 0;
//                double z = 0;
//                double test;
//                if (double.TryParse(ptSost[0], out test))
//                    x = test;

//                if (double.TryParse(ptSost[1], out test))
//                    y = test;

//                if (double.TryParse(ptSost[2], out test))
//                    z = test;
//                return new Vector3d(x, y, z);
//            }
//            return base.ConvertFrom(context, culture, value);
//        }

//        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
//        {
//            if (destinationType == typeof(string))
//                return true;
//            return false;
//        }

//        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
//        {
//            string vecString = String.Empty;
//            if (value is Vector3d)
//            {
//                Vector3d vec = (Vector3d)value;
//                string xStr = vec.X.ToString();
//                string yStr = vec.Y.ToString();
//                string zStr = vec.Z.ToString();
//                vecString = xStr + ";" + yStr + ";" + zStr;
//            }

//            return vecString;
//        }
//    }
//    #endregion

//    #region ��������� CoordinateSystem3dTypeConverter
//    /// <summary>
//    /// ��������������� �������� Vector3d
//    /// </summary>
//    public class CoordinateSystem3dTypeConverter : TypeConverter
//    {
//        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
//        {
//            if (sourceType == typeof(string))
//                return true;
//            return false;
//        }

//        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
//        {
//            if (value is String)
//            {
//                string[] csStringSplit = (value as String).Split('|');
//                if (csStringSplit.Length == 3)
//                {
//                    // ��������������� ��� ����� � �������
//                    Point3dTypeConverter ptConv = new Point3dTypeConverter();
//                    Vector3dTypeConverter ptVect = new Vector3dTypeConverter();
//                    Point3d ptOrigin = (Point3d)ptConv.ConvertFromString(csStringSplit[0]);
//                    Vector3d xVec = (Vector3d)ptVect.ConvertFromString(csStringSplit[1]);
//                    Vector3d yVec = (Vector3d)ptVect.ConvertFromString(csStringSplit[2]);

//                    return new CoordinateSystem3d(ptOrigin, xVec, yVec);
//                }
//            }
//            return base.ConvertFrom(context, culture, value);
//        }

//        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
//        {
//            if (destinationType == typeof(string))
//                return true;
//            return false;
//        }

//        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
//        {

//            if (value is CoordinateSystem3d)
//            {
//                CoordinateSystem3d cs = (CoordinateSystem3d)value;
//                Point3dTypeConverter ptConv = new Point3dTypeConverter();
//                Vector3dTypeConverter ptVect = new Vector3dTypeConverter();

//                string ptString = ptConv.ConvertToString(cs.Origin);
//                string xVecString = ptVect.ConvertToString(cs.Xaxis);
//                string yVecString = ptVect.ConvertToString(cs.Yaxis);

//                StringBuilder sb = new StringBuilder(5);
//                sb.Append(ptString);
//                sb.Append("|");
//                sb.Append(xVecString);
//                sb.Append("|");
//                sb.Append(yVecString);
//                return sb.ToString();
//            }
//            return String.Empty;
//        }
//    }
//    #endregion
//}
