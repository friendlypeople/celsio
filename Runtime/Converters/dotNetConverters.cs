// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Celsio.Runtime.Converters
{
	#region ��������� BooleanTypeConverter
	/// <summary>
	/// ��������������� �������� bool
	/// </summary>
	class BooleanTypeConverter : BooleanConverter
	{
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			if (destType == typeof(string))
				return (bool)value ? "��" : "���";
			return base.ConvertTo(context, culture, value, destType);
		}
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is String)
			{
				string str = value as string;
				if (str.ToUpper() == "��" || str.ToUpper() == "YES" || str.ToUpper() == "TRUE" || str == "1")
					return true;
				if (str.ToUpper() == "���" || str.ToUpper() == "NO" || str.ToUpper() == "FALSE" || str == "0")
					return false;
			}
			return base.ConvertFrom(context, culture, value);
		}
	}
	#endregion

	#region ��������� DoubleTypeConverter
	/// <summary>
	/// ��������������� �������� double �� ���������� �� . ��� ,
	/// </summary>
	public class DoubleTypeConverter : DoubleConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof(string))
				return true;
			return base.CanConvertFrom(context, sourceType);
		}
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is String)
			{
				string sVal = (string)value;
				//������� ����������� ��������
				//String separator = System.Globalization.NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator;
				String separator = NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
				string newSVal = Regex.Replace(sVal.Trim(), "[,.]", separator);
				double res;
				if (double.TryParse(newSVal, NumberStyles.Any, culture, out res))
					return res;
				return null;
			}
			if (value is double)
				return (double)value;

			return base.ConvertFrom(context, culture, value);
		}
	}
	#endregion

	#region ��������� SystemTypeConverter
	/// <summary>
	/// ��������������� �������� System.Type
	/// </summary>
	public class SystemTypeConverter : TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof(string))
				return true;
			return false;
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is String)
			{
				// �������� ������ �������������� ���
				string typeString = (string)value;
				try
				{
					Type type = Type.GetType(typeString, true);
					return type;
				}
				catch (Exception ex)
				{
					System.Diagnostics.Debug.WriteLine("���������� ������������� String(" + typeString + ") � Type: " + ex.Message);
					return String.Empty;
				}
			}
			return base.ConvertFrom(context, culture, value);
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof(string))
				return true;
			return false;
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (value is Type)
			{
				Type type = (Type)value;
				return type.AssemblyQualifiedName;
			}
			return string.Empty;
		}
	}
	#endregion

	#region ��������� EnumTypeConverter
	/// <summary>
	/// ��������������� �������� Enum � ������ � ������ �������� Description
	/// </summary>
	public class EnumTypeConverter : EnumConverter
	{
		public static TypeConverter CreateConverter(Type type)
		{
			return new EnumTypeConverter(type);
		}

		private readonly Type _enumType;
		/// <summary>�������������� ���������</summary>
		/// <param name="type">��� Enum</param>
		public EnumTypeConverter(Type type)
			: base(type)
		{
			_enumType = type;
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
		{
			return destType == typeof(string);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			FieldInfo fi = _enumType.GetField(Enum.GetName(_enumType, value));
			DescriptionAttribute dna = (DescriptionAttribute)Attribute.GetCustomAttribute(fi, typeof(DescriptionAttribute));

			if (dna != null)
				return dna.Description;
			return value.ToString();
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type srcType)
		{
			return srcType == typeof(string);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			foreach (FieldInfo fi in _enumType.GetFields())
			{
				DescriptionAttribute dna =
				  (DescriptionAttribute)Attribute.GetCustomAttribute(
					fi, typeof(DescriptionAttribute));

				if ((dna != null) && ((string)value == dna.Description))
					return Enum.Parse(_enumType, fi.Name);
			}

			return Enum.Parse(_enumType, (string)value);
		}
	}
	#endregion

	#region ��������� ColorTypeConverter
	public class ColorTypeConverter : TypeConverter
	{
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is String)
			{
				string str = value as String;
				string[] numberRgb = str.Split(';');
				//System.Diagnostics.Debug.Assert(numberRgb.Length == 4, "�������� ������ RGB �����");
				int r = Convert.ToInt32(numberRgb[0]);
				int g = Convert.ToInt32(numberRgb[1]);
				int b = Convert.ToInt32(numberRgb[2]);
				return Color.FromArgb(255, r, g, b);
			}
			return Color.FromArgb(255, 0, 0, 0);
		}
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			if (value is Color && destType == typeof(string))
			{
				StringBuilder sb = new StringBuilder();
				Color col = (Color)value;
				sb.Append(col.R.ToString());
				sb.Append(";");
				sb.Append(col.G.ToString());
				sb.Append(";");
				sb.Append(col.B.ToString());
				sb.Append(";");
				sb.Append(255.ToString());
				return sb.ToString();
			}
			return "";
		}
	}
	#endregion
}
