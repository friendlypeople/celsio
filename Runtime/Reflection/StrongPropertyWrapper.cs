// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Reflection;

namespace Celsio.Runtime.Reflection
{
    public sealed class StrongPropertyWrapper
    {
        private delegate T      GetterHandler<T>();
        private delegate void   SetterHandler<T>(T value);

        private readonly object _obj;
        private readonly Dictionary<string, Delegate> _getterMap = new Dictionary<string, Delegate>();
        private readonly Dictionary<string, Delegate> _setterMap = new Dictionary<string, Delegate>();


        public StrongPropertyWrapper(object obj)
        {
            _obj = obj;
            Initialize();
        }

        public bool ContainsSetter(string name)
        {
            return _setterMap.ContainsKey(name);
        }

        public bool ContainsGetter(string name)
        {
            return _getterMap.ContainsKey(name);
        }

        private void Initialize()
        {
            Type type = _obj.GetType();
            foreach (PropertyInfo pi in type.GetProperties())
            {
                if (pi.CanRead && !_getterMap.ContainsKey(pi.Name))
                {
                    Delegate del = CreateGetterDelegate(pi);
                    _getterMap.Add(pi.Name, del);
                }

                if (pi.CanWrite && !_setterMap.ContainsKey(pi.Name))
                {
                    Delegate del = CreateSetterDelegate(pi);
                    _setterMap.Add(pi.Name, del);
                }
            }
        }

        private Delegate CreateGetterDelegate(PropertyInfo pi)
        {
            MethodInfo genericHelper = GetType().GetMethod("CreateGetterDelegateHelper", BindingFlags.Static | BindingFlags.NonPublic);
            MethodInfo constructedHelper = genericHelper.MakeGenericMethod(pi.PropertyType);
            object o = constructedHelper.Invoke(null, new[] {pi, _obj});
            return (Delegate)o;
        }

        private Delegate CreateSetterDelegate(PropertyInfo pi)
        {
            MethodInfo genericHelper = GetType().GetMethod("CreateSetterDelegateHelper", BindingFlags.Static | BindingFlags.NonPublic);
            MethodInfo constructedHelper = genericHelper.MakeGenericMethod(pi.PropertyType);
            object o = constructedHelper.Invoke(null, new[] { pi, _obj });
            return (Delegate)o;
        }


        // Не удалять под страхом смерти! Этот метод вызывается через рефлексию
        private static Delegate CreateGetterDelegateHelper<T>(PropertyInfo pi, object obj)
        {
            return Delegate.CreateDelegate(typeof(GetterHandler<T>), obj, "get_" + pi.Name);
        }

        // Не удалять под страхом смерти! Этот метод вызывается через рефлексию
        private static Delegate CreateSetterDelegateHelper<T>(PropertyInfo pi, object obj)
        {
            return Delegate.CreateDelegate(typeof(SetterHandler<T>), obj, "set_" + pi.Name);
        }



        public object this[string propName]
        {
            get { return _getterMap.ContainsKey(propName) ? _getterMap[propName].DynamicInvoke(null) : null; }
            set
            {
                if (_setterMap.ContainsKey(propName))
                    _setterMap[propName].DynamicInvoke(value);
            }
        }
    }
}
