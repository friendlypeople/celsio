// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.Text;

namespace Celsio.Win32
{
	/// <summary>
	/// Собщения Windows
	/// </summary>
	public static class Messages
	{
		/// <summary>
		/// Изменение размера окна
		/// </summary>
		/// <remarks>Значение WM_SIZE = 0x0005</remarks>
		public const int WM_SIZE = 0x0005;

		/// <summary>
		/// Перемещение окна
		/// </summary>
		/// <remarks>Значение WM_MOVE = 0x0003</remarks>
		public const int WM_MOVE = 0x0003;

		/// <summary>
		/// Активация окна
		/// </summary>
		/// <remarks>Значение WM_ACTIVATE = 0x0006</remarks>
		public const int WM_ACTIVATE = 0x0006;

		/// <summary>
		/// Двойной клик левой клавишей мыши
		/// </summary>
		/// <remarks>Значение WM_LBUTTONDBLCLK = 0x0203</remarks>
		public const int WM_LBUTTONDBLCLK = 0x0203;

		/// <summary>
		/// Нажата левая кнопка мыши
		/// </summary>
		/// <remarks>Значение WM_LBUTTONDOWN = 0x0201</remarks>
		public const int WM_LBUTTONDOWN = 0x0201;

		/// <summary>
		/// Отпущена левая кнопка мыши
		/// </summary>
		/// <remarks>Значение WM_LBUTTONUP = 0x0202</remarks>
		public const int WM_LBUTTONUP = 0x0202;

		/// <summary>
		/// Нажата правая кнопка мыши
		/// </summary>
		/// <remarks>Значение WM_RBUTTONDOWN = 0x0204</remarks>
		public const int WM_RBUTTONDOWN = 0x0204;

		/// <summary>
		/// Отпущена правая кнопка мыши
		/// </summary>
		/// <remarks>Значение WM_RBUTTONUP = 0x0205</remarks>
		public const int WM_RBUTTONUP = 0x0205;

		/// <summary>
		/// Двойной клик правой клавишей мыши
		/// </summary>
		/// <remarks>Значение WM_RBUTTONDBLCLK = 0x206</remarks>
		public const int WM_RBUTTONDBLCLK = 0x206;

		/// <summary>
		/// Двойной клик средней клавишей мыши
		/// </summary>
		/// <remarks>Значение WM_MBUTTONDBLCLK = 0x209</remarks>
		public const int WM_MBUTTONDBLCLK = 0x209;

		/// <summary>
		/// Нажата средняя клавиша мыши
		/// </summary>
		/// <remarks>Значение WM_MBUTTONDOWN = 0x207</remarks>
		public const int WM_MBUTTONDOWN = 0x207;

		/// <summary>
		/// Отпущена средняя клавиша мыши
		/// </summary>
		/// <remarks>Значение WM_MBUTTONUP = 0x208</remarks>
		public const int WM_MBUTTONUP = 0x208;

		/// <summary>
		/// Закрытие окна
		/// </summary>
		/// <remarks>Значение WM_CLOSE = 0x0010</remarks>
		public const int WM_CLOSE = 0x0010;

		/// <summary>
		/// Оповещение родительского окна
		/// </summary>
		/// <remarks>Значение WM_PARENTNOTIFY = 0x210</remarks>
		public const int WM_PARENTNOTIFY = 0x210;


		/// <summary>
		/// Нажата левая кнопка мыши вне клиентской области
		/// </summary>
		/// <remarks>Значение WM_NCLBUTTONDOWN = 0x00A1</remarks>
		public const int WM_NCLBUTTONDOWN = 0x00A1;

		/// <summary>
		/// Отпущена левая кнопка мыши вне клиентской области
		/// </summary>
		/// <remarks>Значение WM_NCLBUTTONUP = 0x00A2</remarks>
		public const int WM_NCLBUTTONUP = 0x00A2;

		/// <summary>
		/// Двойной щелчек левой кнопкой мыши вне клиентской области
		/// </summary>
		/// <remarks>Значение WM_NCLBUTTONDBLCLK = 0x00A3</remarks>
		public const int WM_NCLBUTTONDBLCLK = 0x00A3;

		/// <summary>
		/// Нажата правая кнопка мыши вне клиентской области
		/// </summary>
		/// <remarks>Значение WM_NCRBUTTONDOWN = 0x00A4</remarks>
		public const int WM_NCRBUTTONDOWN = 0x00A4;

		/// <summary>
		/// Отпущена правая кнопка мыши вне клиентской области
		/// </summary>
		/// <remarks>Значение WM_NCRBUTTONUP = 0x00A5</remarks>
		public const int WM_NCRBUTTONUP = 0x00A5;

		/// <summary>
		/// Двойной щелчек правой кнопкой мыши вне клиентской области
		/// </summary>
		/// <remarks>Значение WM_NCRBUTTONDBLCLK = 0x00A6</remarks>
		public const int WM_NCRBUTTONDBLCLK = 0x00A6;

		/// <summary>
		/// Нажата средняя кнопка мыши вне клиентской области
		/// </summary>
		/// <remarks>Значение WM_NCMBUTTONDOWN = 0x00A7</remarks>
		public const int WM_NCMBUTTONDOWN = 0x00A7;

		/// <summary>
		/// Отпущена средняя кнопка мыши вне клиентской области
		/// </summary>
		/// <remarks>Значение WM_NCMBUTTONUP = 0x00A8</remarks>
		public const int WM_NCMBUTTONUP = 0x00A8;

		/// <summary>
		/// Двойной щелчек средней кнопкой мыши вне клиентской области
		/// </summary>
		/// <remarks>Значение WM_NCMBUTTONDBLCLK = 0x00A9</remarks>
		public const int WM_NCMBUTTONDBLCLK = 0x00A9;
		
	}
}
