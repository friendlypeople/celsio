// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System.Collections.Generic;
using Celsio.Serializers.AbnormalSerializer;

namespace Celsio.Data
{
	/// <summary>
	/// Класс описывающий пару связанных данных
	/// </summary>
	/// <typeparam name="T1">Тип первого элемента пары</typeparam>
	/// <typeparam name="T2">Тип второго элемента пары</typeparam>
	[AbnormalSerialize("pair")]
	public class Pair<T1, T2>
	{
		[SerializeDeclarate]
		private T1 _first;

		[SerializeDeclarate]
		private T2 _second;

		/// <summary>
		/// Инициализация экземпляра класса <see cref="Pair&lt;T1, T2&gt;"/>.
		/// </summary>
		public Pair() { }

		/// <summary>
		/// Инициализация экземпляра класса <see cref="Pair&lt;T1, T2&gt;"/>.
		/// </summary>
		/// <param name="first">Первый элемент пары</param>
		/// <param name="second">Второй элемент пары</param>
		public Pair(T1 first, T2 second)
		{
			_first = first;
			_second = second;
		}

		public Pair(KeyValuePair<T1, T2> kvp)
		{
			_first = kvp.Key;
			_second = kvp.Value;
		}

		/// <summary>
		/// Доступ ко второму элементу пары
		/// </summary>
		/// <value>Первый элемент пары</value>
		public T2 Second
		{
			get { return _second; }
			set { _second = value; }
		}

		/// <summary>
		/// Доступ к первому элементу пары
		/// </summary>
		/// <value>Первый элемент пары</value>
		public T1 First
		{
			get { return _first; }
			set { _first = value; }
		}

		/// <summary>
		/// Поменять местами данные пары
		/// </summary>
		/// <returns>Пара связанных данных</returns>
		public Pair<T2, T1> Swap()
		{
			return new Pair<T2, T1>(Second, First);
		}

		public KeyValuePair<T1, T2> ToKeyValuePair()
		{
			return new KeyValuePair<T1, T2>(First, Second);
		}

		public override string ToString()
		{
			return _first + ";" + _second;
		}

		public override int GetHashCode()
		{
			return ToString().GetHashCode();
		}
	}
}
