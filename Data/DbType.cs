// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using Celsio.Patterns;
using Celsio.Serializers.AbnormalSerializer;

namespace Celsio.Data
{
    [AbnormalSerialize]
    class DbInt : DbTypeBase<long>
    {
        public DbInt()
        {
            _supportedTypes.Add(typeof(long));
            _supportedTypes.Add(typeof(int));
            _supportedTypes.Add(typeof(short));
            _supportedTypes.Add(typeof(byte));
			_supportedTypes.Add(typeof(decimal));

            _supportedTypes.Add(typeof(ulong));
            _supportedTypes.Add(typeof(uint));
            _supportedTypes.Add(typeof(ushort));
            _supportedTypes.Add(typeof(char));
			

            _value = 0;
        }

        public override IDbType Copy()
        {
            DbInt dbInt = new DbInt();
            dbInt._value = _value;
            return dbInt;
        }
		public override object Value
		{
			set { _value = Convert.ToInt64(value); }
		}

        public override bool IsEqual(IDbType dbObj)
        {
            if (!(dbObj.Value is long))
                return false;

            long dbObjValue = (long)dbObj.Value;

            return _value == dbObjValue;
        }

    }

    [AbnormalSerialize]
    class DbString : DbTypeBase<string>
    {
        public DbString()
        {
            _supportedTypes.Add(typeof(string));

            _value = string.Empty;
        }

        public override IDbType Copy()
        {
            DbString dbString = new DbString();
            dbString._value = _value;
            return dbString;
        }

        public override bool IsEqual(IDbType dbObj)
        {
            if (!(dbObj.Value is string))
                return false;

            string dbObjValue = (string)dbObj.Value;

            return _value == dbObjValue;
        }
    }

    [AbnormalSerialize]
    class DbBool : DbTypeBase<bool>
    {
        public DbBool()
        {
            _supportedTypes.Add(typeof(bool));

            _value = false;
        }

        public override IDbType Copy()
        {
            DbBool dbBool = new DbBool();
            dbBool._value = _value;
            return dbBool;
        }

        public override bool IsEqual(IDbType dbObj)
        {
            if (!(dbObj.Value is bool))
                return false;

            bool dbObjValue = (bool)dbObj.Value;

            return _value == dbObjValue;
        }
    }

    [AbnormalSerialize]
    class DbDouble : DbTypeBase<double>
    {
        public DbDouble()
        {
            _supportedTypes.Add(typeof(double));
            _supportedTypes.Add(typeof(float));
			_supportedTypes.Add(typeof(long));
			_supportedTypes.Add(typeof(int));
			_supportedTypes.Add(typeof(short));
			_supportedTypes.Add(typeof(byte));

			_supportedTypes.Add(typeof(ulong));
			_supportedTypes.Add(typeof(uint));
			_supportedTypes.Add(typeof(ushort));

            _value = 0;
        }

		public override object Value
		{
			set { base.Value = Convert.ToDouble(value); }
		}

        public override bool IsEqual(IDbType dbObj)
        {
            if (!(dbObj.Value is double))
                return false;

            double dbObjValue = (double)dbObj.Value;

            if (double.IsNaN(_value) && double.IsNaN(dbObjValue))
                return true;

            if (_value != dbObjValue)
                return false;

            return true;
        }

        public override IDbType Copy()
        {
            DbDouble dbDouble = new DbDouble();
            dbDouble._value = _value;
            return dbDouble;
        }
    }

    [AbnormalSerialize]
    class DbBlob : DbTypeBase<byte[]>
    {
        public DbBlob()
        {
            _supportedTypes.Add(typeof(byte[]));

            _value = new byte[0];
        }

        public override IDbType Copy()
        {
            DbBlob dbBlob = new DbBlob();
            dbBlob._value = (byte[]) _value.Clone();
            return dbBlob;
        }

        public override bool IsEqual(IDbType dbObj)
        {
            if (!(dbObj.Value is byte[]))
                return false;

            byte[] dbObjValue = (byte[])dbObj.Value;

            return _value == dbObjValue;
        }
    }

    class DbTypeFactory
    {
        private readonly ParametricClassFactory<DbDataType, IDbType> _factory = new ParametricClassFactory<DbDataType, IDbType>();

        public DbTypeFactory()
        {
            _factory.AddProduct<DbString>(DbDataType.String);
            _factory.AddProduct<DbInt>(DbDataType.Long);
            _factory.AddProduct<DbBool>(DbDataType.Bool);
            _factory.AddProduct<DbDouble>(DbDataType.Double);
            _factory.AddProduct<DbBlob>(DbDataType.Blob);
        }

        public IDbType CreateProduct(DbDataType type)
        {
            return _factory.CreateProduct(type);
        }
    }
}
