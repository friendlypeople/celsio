// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;

namespace Celsio.Data
{
	/// <summary>
	/// ����� ������ �������
	/// </summary>
	public class TableRow
	{
		private readonly List<TableColumn> _columns = new List<TableColumn>();

		/// <summary>
		/// ���������� ���������� �� ��������� ������� � ������
		/// </summary>
		/// <param name="col">��� �������</param>
		/// <returns>
		///   <c>true</c> ���� ������� � ��������� ������ ������������, ����� <c>false</c>.
		/// </returns>
		public bool HasColumn(string col)
		{
		    foreach (TableColumn column in _columns)
		        if (column.Name.Equals(col))
		            return true;
		    return false;
		}

		/// <summary>
		/// �������� �������
		/// </summary>
		public void AddColumn(string col)
		{
			_columns.Add(new TableColumn(col));
		}

		/// <summary>
		/// �������� �������
		/// </summary>
		public void AddColumn(string col, Type type)
		{
			TableColumn tc = new TableColumn(col);
			tc.DataType = type;
			_columns.Add(tc);
		}

		/// <summary>
		/// ���������� �������
		/// </summary>
		public int ColumnCount
		{
			get { return _columns.Count; }
		}

		/// <summary>
		/// ������ �������
		/// </summary>
		public List<TableColumn> Columns
		{
			get { return _columns; }
		}

		/// <summary>
		/// ������ � �������� ������ �� �����
		/// </summary>
		public object this[string name]
		{
			get { return GetCell(name); }
		}

		/// <summary>
		/// ������ � ������ �� �������
		/// </summary>
		public object this[int index]
		{
			get { return GetCell(index); }
		}

		/// <summary>
		/// ��������� �������� ������������ � ������ �� �������
		/// </summary>
		/// <param name="index">������ ������</param>
		/// <returns>�������� ����������� � ������</returns>
		public object GetCell(int index)
		{
			CheckIndex(index);
			return _columns[index].Value;
		}

		/// <summary>
		/// ��������� �������� ������������ � ������ �� �����
		/// </summary>
		/// <param name="name">��� �������.</param>
		/// <returns>�������� ����������� � ������</returns>
		public object GetCell(string name)
		{
			int index = GetIndex(name);
			return GetCell(index);
		}

		/// <summary>
		/// ���������� �������� � ������ �� �������
		/// </summary>
		/// <param name="index">������ ������.</param>
		/// <param name="value">�������� ��������� � ������.</param>
		public void SetCell(int index, object value)
		{
			CheckIndex(index);
			_columns[index].Value = value;
		}

		/// <summary>
		/// ���������� �������� � ������ �� �����
		/// </summary>
		/// <param name="name">��� ������.</param>
		/// <param name="value">�������� ��������� � ������.</param>
		public void SetCell(string name, object value)
		{
			int index = GetIndex(name);
			SetCell(index, value);
		}

		private int GetIndex(string name)
		{
			for (int i = 0; i < _columns.Count; i++)
			{
				TableColumn column = _columns[i];
				if (column.Name.Equals(name))
					return i;
			}
			return -1;
		}

		private void CheckIndex(int index)
		{
			if (index < 0 || index >= _columns.Count)
				throw new Exception("�������� ������ �������");
		}
	}
}
