// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Celsio.Runtime.Converters;
using Celsio.Serializers.AbnormalSerializer;

namespace Celsio.Data
{
	[AbnormalSerialize]
	public class DbRecordData
	{
        private static readonly Dictionary<string, DbDataType> _dataTypes = new Dictionary<string, DbDataType>();

		[SerializeDeclarate]
		private string _privateName = string.Empty;

		[SerializeDeclarate]
		private string _publicName = string.Empty;

        [SerializeDeclarate]
        private DbDataType _type = DbDataType.String;

		[SerializeDeclarate]
        private IDbType _value = new DbString();

		private object _tag;

        [SerializeDeclarate]
        private AttributeCollection _attributes = new AttributeCollection();

		[SerializeDeclarate]
		private readonly List<DbRecordData> _items = new List<DbRecordData>();

		private static readonly DbTypeFactory Factory = new DbTypeFactory();

        public DbRecordData() { }

        public DbRecordData(DbDataType type) 
        {
            _type = type;
			_value = Factory.CreateProduct(_type);
        }

        public DbRecordData(DbDataType type, string privateName) : this(type)
		{
			_privateName = privateName;
		}

        public DbRecordData(DbDataType type, string privateName, string publicName, object value) : this(type, privateName, value)
		{
			_publicName = publicName;
		}

        public DbRecordData(DbDataType type, string privateName, object value) : this(type, privateName)
		{
            Value = value;
		}

        static DbRecordData()
        {
            _dataTypes.Add("BLOB", DbDataType.Blob);
            _dataTypes.Add("BOOL", DbDataType.Bool);
            _dataTypes.Add("DOUBLE", DbDataType.Double);
            _dataTypes.Add("LONG", DbDataType.Long);
            _dataTypes.Add("STRING", DbDataType.String);
        }

        /// <summary>
        /// Создать DbRecordData указанного типа.
        /// </summary>
        /// <param name="type">Тип</param>
        /// <returns></returns>
        public static DbRecordData Create(string type)
        {
            DbDataType dataType;
            return _dataTypes.TryGetValue(type.ToUpper(), out dataType) ? new DbRecordData(dataType) : null;
        }

        /// <summary>
        /// Создать DbRecordData указанного типа.
        /// </summary>
        /// <param name="type">Тип</param>
        /// <param name="privateName">privateName.</param>
        /// <returns></returns>
        public static DbRecordData Create(string type, string privateName)
        {
            DbDataType dataType;
            return _dataTypes.TryGetValue(type.ToUpper(), out dataType) ? new DbRecordData(dataType, privateName) : null;
        }

	    public DbRecordData this[string name]
		{
			get
			{
				foreach (DbRecordData data in _items)
				{
					if (data.PrivateName == name)
						return data;
				}
				return null;
			}
		}

		public string PrivateName
		{
			get { return _privateName; }
			set { _privateName = value; }
		}

        public DbDataType Type
		{
			get { return _type; }
		}

		public List<DbRecordData> Items
		{
			get { return _items; }
		}

		public object Value
		{
			get { return _value.Value; }
			set
			{
			    Type type = value.GetType();
				if (_value.CheckType(type))
					_value.Value = value;
				else
					throw new Exception("Запись таблицы не поддерживает данный тип объекта.");
			}
		}

        public T GetValue<T>()
        {
            Type typeT = typeof(T);
            if (typeT.Equals(_value.SystemType))
                return (T)_value.Value;
            TypeConverter converter = TypeConverterProvider.GetSpecificConverter(_value.SystemType);
            if (converter.CanConvertTo(typeT))
                return (T)converter.ConvertTo(_value.Value, typeT);
            converter = TypeConverterProvider.GetSpecificConverter(typeT);
            if (converter.CanConvertFrom(_value.SystemType))
                return (T)converter.ConvertFrom(_value.Value);
            return (T)_value.Value;
        }

		public string PublicName
		{
			get { return String.IsNullOrEmpty(_publicName) ? _privateName : _publicName; }
			set { _publicName = value; }
		}

		public object Tag
		{
			get { return _tag; }
			set { _tag = value; }
		}

        public Type SystemType
	    {
	        get { return _value.SystemType; }
	    }

        public AttributeCollection Attributes
	    {
	        get { return _attributes; }
	    }

		public DbRecordData Copy()
        {
            DbRecordData newData = new DbRecordData();
            newData._privateName = _privateName;
            newData._publicName = _publicName;
            newData._tag = _tag;
            newData._type = _type;
            newData._value = _value.Copy();
            newData._attributes = _attributes.Copy();
            // Копирование дочерних элементов
		    List<DbRecordData> copiedItems = new List<DbRecordData>();
            foreach (DbRecordData item in Items)
                copiedItems.Add(item.Copy());
            newData.Items.AddRange(copiedItems);

            return newData;
        }

        public bool IsEqualByValue(DbRecordData recordData)
        {
            return _value.IsEqual(recordData._value);
        }
	}
}
