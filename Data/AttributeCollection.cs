// Copyright (c) 2012 Виталий Сайдин, Вячеслав Ишутин

// Данная лицензия разрешает лицам, получившим копию данного программного обеспечения и
// сопутствующей документации (в дальнейшем именуемыми «Программное Обеспечение»), безвозмездно
// использовать Программное Обеспечение без ограничений, включая неограниченное право на использование,
// копирование, изменение, добавление, публикацию, распространение, сублицензирование и/или продажу копий
// Программного Обеспечения, также как и лицам, которым предоставляется данное Программное Обеспечение,
// при соблюдении следующих условий:

// Указанное выше уведомление об авторском праве и данные условия должны быть включены во все копии или
// значимые части данного Программного Обеспечения.

// ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ
// ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ГАРАНТИЯМИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ
// НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ ПРАВ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ
// ПО ИСКАМ О ВОЗМЕЩЕНИИ УЩЕРБА, УБЫТКОВ ИЛИ ДРУГИХ ТРЕБОВАНИЙ ПО ДЕЙСТВУЮЩИМ КОНТРАКТАМ, ДЕЛИКТАМ ИЛИ ИНОМУ,
// ВОЗНИКШИМ ИЗ, ИМЕЮЩИМ ПРИЧИНОЙ ИЛИ СВЯЗАННЫМ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ИЛИ ИСПОЛЬЗОВАНИЕМ ПРОГРАММНОГО
// ОБЕСПЕЧЕНИЯ ИЛИ ИНЫМИ ДЕЙСТВИЯМИ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.


// Copyright (c) 2012 Vitaliy Saydin, Vyacheslav Ishutin

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
// associated documentation files (the "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 


using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Celsio.Runtime.Converters;
using Celsio.Serializers.AbnormalSerializer;

namespace Celsio.Data
{
    [AbnormalSerialize]
    public class AttributeCollection : IEnumerable<Pair<string, string>>
    {
        [SerializeDeclarate]
        private readonly Dictionary<string, string> _attributes = new Dictionary<string, string>();

        public bool Has(string attr)
        {
            return _attributes.ContainsKey(attr);
        }

        public bool HasAttributeWithValue(string attr, string value)
        {
            if (_attributes.ContainsKey(attr))
                return _attributes[attr].Equals(value);
            return false;
        }

        public void Add<T>(string attr, T value)
        {
            TypeConverter tc = TypeConverterProvider.GetSpecificConverter<T>();
            if (tc == null) throw new Exception("Не найден конвертер для типа " + typeof(T));
            object convertTo = tc.ConvertTo(value, typeof(string));
            Add(attr, convertTo as string);
        }

        public void Add(string attr, string value)
        {
            _attributes[attr] = value;
        }

        public string this[string attr]
        {
            get { return GetValue(attr); }
        }

        //public Pair<string, string> GetAttribute(string attr)
        //{
        //    foreach (Pair<string, string> pair in _attributes)
        //    {
        //        if (pair.First.Equals(attr))
        //            return pair;
        //    }
        //    return null;
        //}

        public string GetValue(string attr)
        {
            if(_attributes.ContainsKey(attr))
                return _attributes[attr];
            return string.Empty;
        }

        public T GetValue<T>(string attr)
        {
            string ret = GetValue(attr);

            TypeConverter tc = TypeConverterProvider.GetSpecificConverter<T>();
            if (tc == null) throw new Exception("Не найден конвертер для типа " + typeof(T));
            object attribute = tc.ConvertFrom(ret);
            return (T)attribute;
        }

        public void SetValue(string attr, string value)
        {
            Add(attr, value);
        }

        public void SetValue<T>(string attr, T value)
        {
            Add(attr, value);
        }

        public AttributeCollection Copy()
        {
            AttributeCollection ac = new AttributeCollection();
            foreach(KeyValuePair<string, string> pair in _attributes)
                ac.Add(pair.Key, pair.Value);
            return ac;
        }

        public void Clear()
        {
            _attributes.Clear();
        }

        public IEnumerator<Pair<string, string>> GetEnumerator()
        {
            foreach (KeyValuePair<string, string> pair in _attributes)
                yield return new Pair<string, string>(pair.Key, pair.Value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count
        {
            get { return _attributes.Count; }
        }
    }
}
